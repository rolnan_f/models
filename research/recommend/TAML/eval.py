# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore as ms
from model import TAML
from get_dataset import get_ccp
import model_config as cfg
from sklearn.metrics import roc_auc_score
from tqdm import tqdm
import joblib

if __name__ == '__main__':
    seed = cfg.seed
    ms.set_seed(seed)
    ms.set_context(mode=ms.PYNATIVE_MODE)

    enum_path = 'data/ctrcvr_enum.pkl'
    feats_columns = [
        '101',
        '121',
        '122',
        '124',
        '125',
        '126',
        '127',
        '128',
        '129',
        '205',
        '206',
        '207',
        '216',
        '508',
        '509',
        '702',
        '853',
        '301']
    voc = joblib.load(enum_path)
    feats_conf = [len(voc[feat]) + 1 for feat in feats_columns]
    model = TAML(cfg, feats_conf)
    param_dict = ms.load_checkpoint("./TAML.ckpt")
    ms.load_param_into_net(model, param_dict)

    test_dataset = get_ccp(cfg.test_datapath, cfg.batchsize)


    def test_loop(dataset):
        num_batches = dataset.get_dataset_size()
        model.set_train(False)
        test_loss = 0
        click_true, click_pred, purchase_true, purchase_pred = [], [], [], []
        for data, label_ctr, label_ctcvr in tqdm(dataset.create_tuple_iterator(num_epochs=1)):
            loss, pred_ctr, pred_ctcvr = model(data, label_ctr, label_ctcvr)
            test_loss += loss.numpy()
            click_true.extend(label_ctr.numpy().tolist())
            purchase_true.extend(label_ctcvr.numpy().tolist())
            click_pred.extend(pred_ctr.numpy().tolist())
            purchase_pred.extend(pred_ctcvr.numpy().tolist())
        test_loss /= num_batches
        auc_click = roc_auc_score(
            y_true=click_true,
            y_score=click_pred)
        auc_purchase = roc_auc_score(
            y_true=purchase_true,
            y_score=purchase_pred)
        print(f"Test: \n auc_click: {auc_click:>.4f}, auc_purchase: {auc_purchase:>.4f}, Avg loss: {test_loss:>8f} \n")


    test_loop(test_dataset)
