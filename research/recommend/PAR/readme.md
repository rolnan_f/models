# Table of Contents

- [Table of Contents](#table-of-contents)
- [PAR Description](#par-description)
- [Model Architecture](#model-architecture)
- [Dataset](#dataset)
- [Environment Requirements](#environment-requirements)
- [Quick Start](#quick-start)
- [Script Description](#script-description)
    - [Script and Sample Code](#script-and-sample-code)
        - [Script Parameters](#script-parameters)
- [Model Description](#model-description)
    - [Performance](#performance)
        - [Evaluation Performance](#evaluation-performance)
- [Description of Random Situation](#desciption-of-random-situation)
- [ModelZoo Homepage](#modelzoo-homepage)

# PAR Description

Page-level Attentional Reranking (PAR) is a page-level reranking method that jointly rerank multiple lists and optimize the overall utility by considering the page-wise information. See the paper [A Bird’s-eye View of Reranking: from List Level to Page Level](https://arxiv.org/pdf/2211.09303.pdf) in WSDM 2023 for more details.

# Model Architecture

PAR is an end-to-end reranking model, consisting of three layers: (1) embedding layer, (2) page-level interaction layer, and (3) score estimation layer.

- **Embedding layer**: The embedding layer converts each candidate or historical item to a dense feature vector.
- **Page-level interaction layer**: The page-level interaction layer includes three modules (i.e., hierarchical dual-side attention, spatial-scaled attention, and dense network) to capture page-wise information.
- **Score estimation layer**: The Multi-gated Mixture-of-Experts are adopted to learn the commonalities and distinctions among different lists and output the predicted score in the score estimation layer.

# Dataset

- [Cloud Theme Click Dataset](https://tianchi.aliyun.com/dataset/9716)

# Environment Requirements

- Hardware(Ascend)
    - Prepare hardware environment with Ascend processor.
- Framework
    - [MindSpore](https://gitee.com/mindspore/mindspore).  For more information, please check the resources below:
        - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/zh-CN/master/index.html)
        - [MindSpore Python API](https://www.mindspore.cn/docs/zh-CN/master/api_python/mindspore.html)

# Quick Start

1. Clone the code

```bash
git clone https://gitee.com/mindspore/models.git
cd models/research/recommend/PAR
```

2. Download the dataset

Please download the dataset at [Cloud Theme Click Dataset](https://tianchi.aliyun.com/dataset/9716)

```bash
mkdir -p data/data/CloudTheme/raw_data && cd data/data/CloudTheme/raw_data
```

Put the data into the ```raw_data``` folder.

3. Preprocess the data

```bash
python preprocess_ctc.py
```

4. Start training

Once the dataset is ready, the model can be trained and evaluated by the command as follows:

```bash
# Python command
python run_par_model.py
```

# Script Description

## Script and Sample Code

```markdown
└── PAR
    ├── README.md                                 # Instruction and tutorial
    ├── click_model.py                            # Generate the user clicks
    ├── model.py                                  # PAR model
    ├── run_PAR_model.py                          # Python script of running the experiments
    ├── dataset.py                                # Create the dataset
    ├── util.py                                   # Other functions used in the data processing or model training
    └── preprocess_CTC.py                         # Preprocess the dataset
```

## Script Parameters

```markdown

Used by: run_PAR_model.py

Arguments:

  --train_data_dir             train dir
  --test_data_dir              test dir
  --valid_data_dir             valid dir
  --stat_dir                   dir for statistic
  --save dir                   dir for saving
  --epoch_num                  epochs of each iteration.(Default:100)
  --batch_size                 batch size.(Default:128)
  --max_hist_len               the max length of history.(Default:30)
  --lr                         learning rate(Default:1e-4)
  --l2_norm                    l2 loss scale (Default:2e-4)
  --keep_prob                  keep probability (Default:0.8)
  --emb_dim                    size of embedding (Default:16)
  --hidd_size                  hidden size(Default:64)
  --d_model                    input dimension of FFN
  --d_inner                    hidden dimension of FFN
  --n_head                     the number of head in self-attention
  --expert_num                 the number of expert in MMoE
  --hidden_layer               size of hidden layer
  --metric_scope               the scope of metrics
  --grad_norm                  max norm of gradient (0 means no limitation)
  --setting_path               setting dir
  --eval_freq                  the frequency of evaluating on the valid set when training
```

# Model Description

## Evaluation Performance

| Framework            | MindSpore                                                            | Tensorflow                                                           |
|----------------------|----------------------------------------------------------------------|----------------------------------------------------------------------|
| Processor            | Ascend                                                               | GPU                                                                  |
| Resource             | 910                                                                  | 3080 Ti                                                              |
| Uploaded Date        | 1/7/2023 (month/day/year)                                            | NA                                                                   |
| Framework Version    | 1.7                                                                  | 1.15                                                                 |
| Dataset              | [Cloud Theme Click Dataset](https://tianchi.aliyun.com/dataset/9716) | [Cloud Theme Click Dataset](https://tianchi.aliyun.com/dataset/9716) |
| Traning Parameter    | Epoch=100,<br />batch_size=128,<br />lr=0.0001 (default)             | Epoch=100,<br />batch_size=128,<br />lr=0.0001(default)              |
| Optimizer            | Adam                                                                 | Adam                                                                 |
| Loss Function        | Sigmoid cross entropy                                                | Sigmoid cross entropy                                                |
| Utility Metric       | 1.4517                                                               | 1.4446                                                               |
| CTR Metric           | 1.4635                                                               | 1.4137                                                               |

## Description of Random Situation

- Shuffle of the dataset.
- Random seeds.
- Initialization of some model weights.
- Dropout operations.

# ModelZoo Homepage

Please check the official [homepage](https://gitee.com/mindspore/models).
