#!/bin/bash
DataPath=$1               # The directory that stores the list data.
OutputPath=$2             # Log directory
StartDay=1
EndDay=30
length=5
budget=0.125
gamma=1
comment=""


for cp in 10341182 30801593 17686799 15398570 5061834 15184511 29427842 28351001 18975823 31772643
do
python -u main.py --campaign $cp --start_day $StartDay --end_day $EndDay \
    --method MKB --data_path $DataPath --output_path $OutputPath --length $length --budget $budget --gamma $gamma \
    --mode search  --comment=$comment

python -u main.py --campaign $cp --start_day $StartDay --end_day $EndDay \
    --method Linear --data_path $DataPath --output_path $OutputPath --length $length --budget $budget --gamma $gamma \
    --mode search  --comment=$comment

python -u main.py --campaign $cp --start_day $StartDay --end_day $EndDay \
    --method S_ORTB --data_path $DataPath --output_path $OutputPath --length $length --budget $budget --gamma $gamma \
    --mode search --comment=$comment

python -u main.py --campaign $cp --start_day $StartDay --end_day $EndDay \
    --method M_ORTB --data_path $DataPath --output_path $OutputPath --length $length --budget $budget --gamma $gamma \
    --mode search --comment=$comment
done