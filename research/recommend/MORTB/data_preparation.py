# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Preprocessing raw data, including CTR/CVR training and list generation.
"""
import argparse
import os
import pickle as pkl
import pandas as pd
import numpy as np


import mindspore as ms
from mindspore import dataset as ds
from mindspore import Model
from mindspore import load_param_into_net
from mindspore.common import set_seed

from src.ctr_cvr_prediction import ModelBuilder
from src.ctr_cvr_prediction import AUCMetric
from src.ctr_cvr_prediction import Dataset

FEATURES = ['campaign', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'cat7', 'cat8', 'cat9']
set_seed(123)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_path', default='/NAS2020/Workspaces/DMGroup/wtou/datasets/Criteo_Attribution/',
                        type=str, help='Directory for the raw dataset')
    parser.add_argument('--mode', default='preprocess', choices=['preprocess', 'train', 'test', 'generate'], type=str,
                        help='\'preprocess\': preparing training data for CTR and CVR prediction\n'
                             '\'train\': training CTR or CVR model\n'
                             '\'test\': testing CTR or CVR model\n'
                             '\'generate\': generating lists for multi-slot landscape')

    # args for CTR and CVR prediction
    parser.add_argument('--model_path', default='cache/saved_models/', type=str, help='Model path for test')
    parser.add_argument('--log_path', default='cache/logs/', type=str, help='Log path for loss and metrics')
    parser.add_argument('--start_test_day', default=25, type=int, help='split train/test data')
    parser.add_argument('--batch_size', default=4096, type=int)
    parser.add_argument('--epoch', default=100, type=int)
    parser.add_argument('--lr', default=0.0001, type=float)
    parser.add_argument('--early_stop_epoch', default=10, type=int)
    parser.add_argument('--eval_callback', default=True, type=bool)
    parser.add_argument('--loss_callback', default=True, type=bool)
    parser.add_argument('--save_checkpoint', default=True, type=bool)
    parser.add_argument('--save_checkpoint_steps', default=1, type=int)
    parser.add_argument('--keep_checkpoint_max', default=1, type=int)
    parser.add_argument('--task', default='ctr', choices=['ctr', 'cvr'], type=str)
    parser.add_argument('--device', default='GPU', type=str)
    parser.add_argument('--device_id', default=0, type=int)

    # args for generating lists
    parser.add_argument('--output_path', default='Logs/temp_data', type=str, help='Output Directory for the '
                                                                                  'generating dataset')
    parser.add_argument('--ctr_model_path', default='', type=str, help='Model path for ctr model')
    parser.add_argument('--cvr_model_path', default='', type=str, help='Model path for cvr model')
    FLAGS, _ = parser.parse_known_args()
    return FLAGS


def preprocess_raw_data(data_path, start_test_day):
    data_file = os.path.join(data_path, 'criteo_attribution_dataset.tsv.gz')
    df = pd.read_csv(data_file, sep='\t', compression='gzip')
    df['day'] = np.floor(df.timestamp / 86400.).astype(int)

    feat2id = {}
    idx = 0
    for feat in FEATURES:
        feats = df[feat].unique()
        for f in feats:
            key = feat + '_' + str(f)
            feat2id[key] = idx
            idx += 1
    print('Feature size: ', len(feat2id))
    with open(os.path.join(data_path, 'feat2id.pkl'), 'wb') as f:
        pkl.dump(feat2id, f)

    def cat2id(x, name):
        return feat2id[name + '_' + str(x)]
    df_train = df[df.day < start_test_day]
    df_test = df[df.day >= start_test_day]

    new_df_train = pd.DataFrame()
    new_df_test = pd.DataFrame()
    for feat in FEATURES:
        new_df_train[feat] = df_train[feat].apply(cat2id, args=(feat,))
        new_df_test[feat] = df_test[feat].apply(cat2id, args=(feat,))

    new_df_train['ctr_label'] = df_train['click']
    new_df_test['ctr_label'] = df_test['click']
    new_df_train['cvr_label'] = df_train['attribution']
    new_df_test['cvr_label'] = df_test['attribution']
    print('Training size: ', len(new_df_train))
    print('Testing size: ', len(new_df_test))
    new_df_train.to_csv(os.path.join(data_path, 'criteo_attribution_dataset.preprocess.train.tsv'), sep='\t')
    new_df_test.to_csv(os.path.join(data_path, 'criteo_attribution_dataset.preprocess.test.tsv'), sep='\t')


def train(args):
    print(args)
    if not os.path.exists(args.model_path):
        os.makedirs(args.model_path)
    if not os.path.exists(args.log_path):
        os.makedirs(args.log_path)
    ms.set_context(mode=ms.PYNATIVE_MODE, device_target=args.device, device_id=args.device_id)
    train_dataset = Dataset(args.data_path, 'train', args.task)
    test_dataset = Dataset(args.data_path, 'test', args.task)
    train_data = ds.GeneratorDataset(source=train_dataset,
                                     column_names=['data', 'label'])
    test_data = ds.GeneratorDataset(source=test_dataset,
                                    column_names=['data', 'label'])
    train_data = train_data.batch(args.batch_size, drop_remainder=True)
    test_data = test_data.batch(args.batch_size, drop_remainder=True)
    mb = ModelBuilder(args)

    model = Model(mb.train_net, eval_network=mb.test_net, metrics={"auc": mb.auc_metric})
    callback_list = mb.get_callback_list(model, test_data)
    model.train(args.epoch, train_data, callbacks=callback_list)


def test(args, path, infer=False):
    ms.set_context(mode=ms.PYNATIVE_MODE, device_target=args.device)
    mb = ModelBuilder(args)
    net = mb.test_net
    # net = LogisticRegression()
    train_auc_metric = AUCMetric()
    test_auc_metric = AUCMetric()
    result = []
    train_dataset = Dataset(args.data_path, 'train', args.task, infer)
    test_dataset = Dataset(args.data_path, 'test', args.task, infer)
    train_data = ds.GeneratorDataset(source=train_dataset, column_names=['data', 'label'], shuffle=False)
    test_data = ds.GeneratorDataset(source=test_dataset, column_names=['data', 'label'], shuffle=False)
    train_data = train_data.batch(args.batch_size)
    test_data = test_data.batch(args.batch_size)
    param_dict = ms.load_checkpoint(path)
    load_param_into_net(net, param_dict)

    for data in train_data.create_dict_iterator():
        data['label'] = data['label'].astype(ms.float32).reshape([-1])
        pred, _ = net(data['data'], data['label'])
        train_auc_metric.update(pred, data['label'])
        result += pred.asnumpy().tolist()

    for data in test_data.create_dict_iterator():
        data['label'] = data['label'].astype(ms.float32).reshape([-1])
        pred, _ = net(data['data'], data['label'])
        test_auc_metric.update(pred, data['label'])
        result += pred.asnumpy().tolist()

    train_auc = train_auc_metric.eval()
    test_auc = test_auc_metric.eval()
    print('%s Auc on train set: %f' % (args.task, train_auc))
    print('%s Auc on test set: %f' % (args.task, test_auc))

    return result


def generate_list(args):
    args.task = 'ctr'
    ctr_pred = test(args, args.ctr_model_path, True)

    args.task = 'cvr'
    cvr_pred = test(args, args.cvr_model_path, True)

    data_path = os.path.join(args.data_path, 'criteo_attribution_dataset.tsv.gz')
    df = pd.read_csv(data_path, sep='\t', compression='gzip')
    df['day'] = np.floor(df.timestamp / 86400.).astype(int)
    df['cvr'] = cvr_pred
    df['bid'] = df['cpo'] * df['cvr']
    df['ctr'] = ctr_pred
    df['ecpm'] = df['ctr'] * df['bid']

    cp = list(df['campaign'].value_counts()[:10].keys())
    print('Campaign ID: ', cp)

    if not os.path.exists(args.output_path):
        os.makedirs(args.output_path)

    for c in cp:
        if not os.path.exists(os.path.join(args.output_path, str(c))):
            os.mkdir(os.path.join(args.output_path, str(c)))
        for day in range(1, 31):
            df_cd = df[(df['campaign'] == c) & (df['day'] == day)].reset_index(drop=True)
            df_v = df[(df['campaign'] != c) & (df['day'] == day)][['ecpm', 'bid', 'ctr', 'cvr', 'click', 'cpo']]
            l = len(df_cd)
            print(c, day, l)
            ss = np.zeros([l, 60])
            for i in range(l):
                l1 = df_v.sample(n=10).sort_values('ecpm', ascending=False)
                ss[i] = np.array(l1).reshape(-1)
            cols = []
            for i in range(1, 11):
                for ele in ['ecpm', 'bid', 'ctr', 'cvr', 'click', 'cpo']:
                    cols.append('p%d_%s' % (i, ele))
            tmp = pd.DataFrame(ss, columns=cols)
            ret = pd.concat([df_cd, tmp], axis=1)
            ret.to_csv(os.path.join(args.output_path, str(c), str(day)), sep='\t', header=True)


if __name__ == '__main__':
    args = parse_args()
    if args.mode == 'preprocess':
        preprocess_raw_data(args.data_path, args.start_test_day)
    elif args.mode == 'train':
        train(args)
    elif args.mode == 'test':
        test(args, args.model_path)
    elif args.mode == 'generate':
        generate_list(args)
    else:
        raise NotImplementedError
