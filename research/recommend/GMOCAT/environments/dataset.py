# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import mindspore as ms

class TrainDataset:
    def __init__(self, data, concept_map, num_students, num_questions, num_concepts):
        self.raw_data = data
        self.concept_map = concept_map
        self.n_students = num_students
        self.n_questions = num_questions
        self.n_concepts = num_concepts

        self.data = {}
        for sid, qid, correct in data:
            self.data.setdefault(sid, {})
            self.data[sid].setdefault(qid, {})
            self.data[sid][qid] = correct

        student_ids = set(x[0] for x in data)
        question_ids = set(x[1] for x in data)
        concept_ids = set(sum(concept_map.values(), []))

        assert max(student_ids) < num_students
        assert max(question_ids) < num_questions
        assert max(concept_ids) < num_concepts

    def __getitem__(self, item):
        sid, qid, score = self.raw_data[item]
        concepts = self.concept_map[qid]
        concepts_emb = [0.] * self.n_concepts
        for concept in concepts:
            concepts_emb[concept] = 1.0
        return sid, qid, ms.Tensor(concepts_emb), ms.Tensor(score, dtype=ms.float32)

    def __len__(self):
        return len(self.raw_data)
