# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
import sys
import environments as all_envs
import agents as all_agents
import function as all_FA
from util import get_objects, set_global_seeds, arg_parser, str2bool

import mindspore as ms
from mindspore import context
from mindspore_gl import GraphField
from mindspore_gl.nn import GNNCell
GNNCell.disable_display()

def common_arg_parser():
    parser = arg_parser()
    parser.add_argument('-seed', type=int, default=145)
    parser.add_argument('-environment', type=str, default="GCATEnv")
    parser.add_argument('-data_path', type=str, default="./data/")
    parser.add_argument('-data_name', type=str, default="assist2009")
    parser.add_argument('-agent', type=str, default="GCATAgent")
    parser.add_argument('-FA', type=str, default="GCAT")
    parser.add_argument('-CDM', type=str, default='IRT')
    parser.add_argument('-T', type=int, default=20)
    parser.add_argument('-ST', type=eval, default="[1,5,10,20]")
    parser.add_argument('-gpu_no', type=str, default="0")

    parser.add_argument('-learning_rate', type=float, default=0.001, help="learning rate")
    parser.add_argument('-training_epoch', type=int, default=50, help="training epoch")
    parser.add_argument('-cdm_lr', type=float, default=0.02, help="cdm lr")
    parser.add_argument('-cdm_epoch', type=int, default=4, help="cdm epoch")
    parser.add_argument('-cdm_bs', type=int, default=128, help="cdm bs")

    parser.add_argument('-train_bs', type=int, default=128)
    parser.add_argument('-test_bs', type=int, default=1024)
    parser.add_argument('-batch', type=int, default=128)

    parser.add_argument('-gamma', type=float, default=0.5)
    parser.add_argument('-latent_factor', type=int, default=256)
    parser.add_argument('-n_block', type=int, default=1)
    parser.add_argument('-graph_block', type=int, default=1)
    parser.add_argument('-n_head', type=int, default=1)
    parser.add_argument('-dropout_rate', type=float, default=0.1)
    parser.add_argument('-policy_epoch', type=int, default=1)
    parser.add_argument('-morl_weights', type=eval, default="[1,1,1]")
    parser.add_argument('-emb_dim', type=int, default=128)
    parser.add_argument('-use_graph', type=str2bool, default="True")
    parser.add_argument('-use_attention', type=str2bool, default="True")
    parser.add_argument('-store_action', type=str2bool, default="False")
    return parser

def build_graph(g_type, n_nodes, path):
    src_list = []
    dst_list = []
    if g_type == 'direct':
        txt_path = path + 'K_Directed.txt'
    elif g_type == 'k_from_e':
        txt_path = path +'k_from_e.txt'
    elif g_type == 'e_from_k':
        txt_path = path +'e_from_k.txt'

    with open(txt_path, 'r') as f:
        for line in f.readlines():
            line = line.replace('\n', '').split('\t')
            src_list.append(int(line[0]))
            dst_list.append(int(line[1]))
    n_edges = len(src_list)
    src_list = ms.Tensor(src_list, dtype=ms.int32)
    dst_list = ms.Tensor(dst_list, dtype=ms.int32)
    g = GraphField(src_list, dst_list, n_nodes, n_edges)
    return g

def construct_local_map(args, path):
    local_map = {
        'directed_g': build_graph('direct', args.know_num, path),
        'k_from_e': build_graph('k_from_e', args.know_num + args.item_num, path),
        'e_from_k': build_graph('e_from_k', args.know_num + args.item_num, path),
    }
    return local_map

def main(args):
    arg_parser = common_arg_parser()
    args, _ = arg_parser.parse_known_args(args)
    args.model = "_".join([args.agent, args.FA, str(args.T)])
    set_global_seeds(args.seed)
    os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu_no)

    context.set_context(mode=context.PYNATIVE_MODE, device_target='CPU')
    device_target = context.get_context('device_target')
    mode = context.get_context('mode')
    print(device_target, mode)

    print("Training Model: "+args.model)

    envs = get_objects(all_envs)
    env = envs[args.environment](args)
    args.user_num = env.user_num
    args.item_num = env.item_num
    args.know_num = env.know_num
    print("Hype-Parameters: "+str(args))
    local_map = construct_local_map(args, path=f'graph_data/{args.data_name}/')
    nets = get_objects(all_FA)
    fa = nets[args.FA].create_model(args, local_map)
    agents = get_objects(all_agents)
    agents[args.agent](env, fa, args).train()

if __name__ == '__main__':
    main(sys.argv)
