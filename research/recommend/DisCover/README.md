# Contents

- [Contents](#contents)
- [DisCover Description](#discover-description)
- [Dataset](#dataset)
- [Environment Requirements](#environment-requirements)
- [Quick Start](#quick-start)
- [Script Description](#script-description)
    - [Script and Sample Code](#script-and-sample-code)
- [Model Description](#model-description)
    - [Performance](#performance)
        - [Training Performance](#training-performance)
        - [Inference Performance](#inference-performance)
- [ModelZoo Homepage](#modelzoo-homepage)

# [DisCover-Description](#contents)

We propose the disentangled music representation learning framework (DisCover) for CSI. DisCover consists of two critical components: (1) Knowledge-guided Disentanglement Module (KDM) and (2) Gradient-based Adversarial Disentanglement Module (GADM), which block intra-version and inter-version biased effects, respectively. KDM minimizes the mutual information between the learned representations and version-variant factors that are identified with prior domain knowledge. GADM identifies version-variant factors by simulating the representation transitions between intra-song versions, and exploits adversarial distillation for effect blocking.

Jiahao Xun, Shengyu Zhang, Yanting Yang, Jieming Zhu, Liqun Deng, Zhou Zhao, Zhenhua Dong, Ruiqi Li, Lichao Zhang, Fei Wu. DisCover: Disentangled Music Representation Learning for Cover Song Identification, in SIGIR 2023, https://arxiv.org/abs/2307.09775

# [Dataset](#contents)

- [SHS100K](https://github.com/NovaFrost/SHS100K2)
- [Covers80](https://labrosa.ee.columbia.edu/projects/coversongs/covers80/)
- Processed data can be found in [here](https://drive.google.com/drive/folders/1h0YzZ7PmOOg5UsPUYgcF5Wp_3GNiA-W5?usp=sharing).

# [Environment Requirements](#contents)

- Hardware（CPU and GPU）
    - Prepare hardware environment with CPU processor and GPU of Nvidia.
- Framework
    - MindSpore-2.0.0
- Requirements
  - numpy
  - tqdm
  - resemblyzer
  - parselmouth
  - mindspore==2.0.0
  - librosa==0.8.0
  - python==3.9

- For more information, please check the resources below:
  - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/en/master/index.html)
  - [MindSpore Python API](https://www.mindspore.cn/docs/en/master/api_python/mindspore.html)

# [Quick Start](#contents)

After installing MindSpore via the official website, you can start training and evaluation as follows:

- Training and Evaluating

  ```shell
  # train and evaluate base model with DisCover
  python train.py --model='CQTNet' --notes='CQTNet_Dis' --dataset='shs100k'
  # evaluate the trained models on SHS100K and Covers80
  python infer.py --model ='CQTNet' --load_model_path='checkpoints/CQTNet_Dis/best.ckpt'
  ```

- Model checkpoint can be found in [here](https://drive.google.com/drive/folders/1h0YzZ7PmOOg5UsPUYgcF5Wp_3GNiA-W5?usp=sharing).

# [Script Description](#contents)

## [Script and Sample Code](#contents)

  ```text
  .
  └─DisCover
    ├─README.md                     # descriptions of DisCover
    ├─data                          # training data
      ├─shs100k_train.txt           # songs in shs100k used for train
      ├─shs100k_valid.txt           # songs in shs100k used for validate
      ├─shs100k_test.txt            # songs in shs100k used for test
      ├─song80_list.txt             # songs in Covers80 used for test
    ├─models                        # code for DisCover model
      ├─__init__.py
      ├─adversarial.py              # the model structure of discriminator
      ├─basic_cell.py               # the basic model structure
      ├─club.py                     # the model structure of vCLUB
      ├─CQTNet.py                   # the model structure of CQTNet
      ├─TPPNet.py                   # the model structure of CQTTPPNet
      ├─DisCover.py                 # the framework of DisCover
      ├─encoder.py                  # the model structure of pitch&timbre encoder
    ├─utils                         # the code of some useful functions
      ├─cqt_loader.py               # the code of dataloader
      ├─pesudo_label_generator.py   # the code of generating pesudo label
      ├─separate_pitch.py           # the code of extracting pitch from .wav
      ├─separate_speaker.py         # the code of extracting timbre from .wav
      ├─utility.py                  # the code of functions for evaluating
      ├─wav2cqt.py                  # the code of extracting CQT feature from .wav
    ├─infer.py                      # evaluate trained base model with DisCover framework
    ├─train.py                      # train and evaluate base model with DisCover framework
  ```

## [Script Parameters](#contents)

- Parameters that can be modified at the terminal

  ```text
  # Train
  model: 'CQTNet'         # base model choice
  batch_size: 32          # training batch size
  num_workers: 2          # numbers of workers used for loading data
  latent_dim: 300         # the dimension of representation
  max_epoch: 100          # training epoch
  lr:0.0004               # training learning rate.
  inner_train_loop: 5     # training epoch of the inner loop
  dataset: 'shs100k'      # training dataset choice
  mi_weight: 0.01         # the hyper-parameter
  weight_decay: 1e-5      # the weight of regularization
  lr_decay: 0.8           # when val_loss increase, lr = lr*lr_decay
  notes: 'test'           # the name for saving model
  ```

# [Model Description](#contents)

## [Performance](#contents)

### Training Performance

| Parameters          | GPU                                                                                                                         |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------|
| Resource            | AMD Ryzen 2990WX 32-Core Processor;256G Memory;NVIDIA GeForce 3090                                                        |
| uploaded Date       | 07/19/2023 (month/day/year)                                                                                                 |
| MindSpore Version   | 2.0.0                                                                                                                       |
| Dataset             | SHS100K & Covers80                                                                                      |
| Training Parameters | epoch=100, batch_size=32, lr=4e-4                                                                                           |
| Optimizer           | Adam                                                                                                                        |
| Loss Function       | CrossEntropy                                                                                                       |
| Outputs             | MAP, P@10, MR1                                                                                                                     |
| Per Epoch Time      | 42 mins                                                                                                                  |

### Inference Performance

| Parameters        | GPU                                                                                                                         |
|-------------------|-----------------------------------------------------------------------------------------------------------------------------|
| Resource          | AMD Ryzen 2990WX 32-Core Processor;256G Memory;NVIDIA GeForce 3090                                                        |
| uploaded Date     | 07/19/2023 (month/day/year)                                                                                                 |
| MindSpore Version | 2.0.0                                                                                                                     |
| Dataset           | SHS100K & Covers80                                                                                            |
| Outputs           | MAP, P@10, MR1                                                                                                                  |
| Per Step Time    | 0.03s                                                                                                   |

# [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models)


