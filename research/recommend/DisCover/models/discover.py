# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from collections import namedtuple
from mindspore import nn
from .basic_cell import BasicCell


class DisCover(BasicCell):
    def __init__(
        self,
        base_model,
        f0_encoder,
        speaker_encoder,
        latent_dim=300,
        temp=5,
        class_num=10000,
    ):
        super().__init__()
        self.base_model = base_model
        self.f0_encoder = f0_encoder
        self.speaker_encoder = speaker_encoder
        self.feature_norm = nn.LayerNorm([latent_dim])
        self.temp = temp
        self.class_num = class_num
        self.latent_dim = latent_dim
        self.classifier = nn.Dense(latent_dim, class_num)
        self.output_class = namedtuple('output', ['score', 'target', 'feature4infer', 'pitch', 'speaker', 'feature'])

    def construct(self, x, pitch_emb=None, speaker_emb=None, infer=False):
        feature = self.base_model(x)
        feature4infer = self.feature_norm(feature)
        if infer:
            return feature4infer
        if pitch_emb is None:
            return self.classifier(feature4infer), feature4infer
        pitch_emb = self.f0_encoder(pitch_emb)
        score = self.classifier(feature4infer + pitch_emb / self.temp)
        if speaker_emb is None:
            return score, feature4infer
        spk_score, spk_emb = self.speaker_encoder(speaker_emb)
        output = self.output_class(score, feature4infer, spk_score, pitch_emb, spk_emb, feature)
        return output
