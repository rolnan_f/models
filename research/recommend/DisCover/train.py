# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================


import os
import stat
import argparse
from collections import namedtuple
import numpy as np
import models
from models import (
    PitchEncoder,
    SpeakerEncoder,
    DisCover,
    Discriminator,
    FirstStageTrainOneStepCell,
    SecondStageTrainOneStepCell,
    CLUBSample,
)
from utils.cqt_loader import CQTpair, CQTsingle
from utils.utility import norm, calc_mean_avg_precision
import mindspore as ms
from mindspore import nn
from mindspore import dataset as ds
from tqdm import tqdm

ms.set_context(device_target="GPU", device_id=0)
FLAGS = os.O_CREAT | os.O_APPEND | os.O_WRONLY
MODES = stat.S_IWUSR | stat.S_IRUSR


def val_slow(model, dataloader, epoch, args, is_test=False, name=None):
    if not os.path.exists("result/"):
        os.mkdir("result/")
    model.set_train(False)
    labels, features = [], []
    for _, (data, label) in tqdm(enumerate(dataloader), total=len(dataloader)):
        feature = model(data, infer=True).asnumpy()
        label = label.asnumpy()
        features.append(feature)
        labels.append(label)
    features = np.squeeze(np.array(features))
    labels = np.squeeze(np.array(labels))
    features = norm(features)
    dis2d = -np.matmul(features, features.T)
    mean_avg_precision, top10, rank1 = calc_mean_avg_precision(dis2d, labels)
    if args.notes:
        log_path = "result/" + args.notes + ".txt"
    if not is_test:
        print("valid: epoch, mean_avg_precision, top10, rank1:")
        print(epoch, mean_avg_precision, top10, rank1)
        with os.fdopen(os.open(log_path, FLAGS, MODES), "a") as f:
            f.write("valid: ep, mean_avg_precision, top10, rank1:\n")
            f.write(
                str(epoch)
                + " "
                + str(mean_avg_precision)
                + " "
                + str(top10)
                + " "
                + str(rank1)
                + "\n"
            )
    else:
        print(name + " test: epoch, mean_avg_precision, top10, rank1:")
        print(epoch, mean_avg_precision, top10, rank1)
        with os.fdopen(os.open(log_path, FLAGS, MODES), "a") as f:
            f.write(name + " test: epoch, mean_avg_precision, top10, rank1:\n")
            f.write(
                str(epoch)
                + " "
                + str(mean_avg_precision)
                + " "
                + str(top10)
                + " "
                + str(rank1)
                + "\n"
            )
    model.set_train(True)
    return mean_avg_precision


def prepare_data(args):
    print("train data preparing...")
    if args.dataset == "shs100k":
        train_data0 = CQTpair("train", out_length=200)
        train_data1 = CQTpair("train", out_length=300)
        train_data2 = CQTpair("train", out_length=400)
        valid_data = CQTsingle("val", out_length=None)
        val_data80 = CQTsingle("songs80", out_length=None)
    else:
        raise NotImplementedError
    print("train data preparing...")
    train_dataloader0 = ds.GeneratorDataset(
        train_data0,
        column_names=["query", "target", "label", "pitch", "speaker", "spk_label"],
        shuffle=True,
        num_parallel_workers=args.num_workers,
    ).batch(args.batch_size)
    train_dataloader1 = ds.GeneratorDataset(
        train_data1,
        column_names=["query", "target", "label", "pitch", "speaker", "spk_label"],
        shuffle=True,
        num_parallel_workers=args.num_workers,
    ).batch(args.batch_size)
    train_dataloader2 = ds.GeneratorDataset(
        train_data2,
        column_names=["query", "target", "label", "pitch", "speaker", "spk_label"],
        shuffle=True,
        num_parallel_workers=args.num_workers,
    ).batch(args.batch_size)
    print("valid data preparing...")
    if args.dataset == "shs100k":
        valid_dataloader = ds.GeneratorDataset(
            valid_data,
            shuffle=False,
            num_parallel_workers=args.num_workers,
            column_names=["query", "label"],
        ).batch(1)
        val_dataloader80 = ds.GeneratorDataset(
            val_data80,
            shuffle=False,
            num_parallel_workers=args.num_workers,
            column_names=["query", "label"],
        ).batch(1)
    else:
        raise NotImplementedError
    output_class = namedtuple(
        "output",
        [
            "train_dataloader0",
            "train_dataloader1",
            "train_dataloader2",
            "valid_dataloader",
            "val_dataloader80",
        ],
    )
    output = output_class(
        train_dataloader0,
        train_dataloader1,
        train_dataloader2,
        valid_dataloader,
        val_dataloader80,
    )
    return output


def train(args):
    model = getattr(models, args.model)(latent_dim=args.latent_dim)
    f0_encoder = PitchEncoder(latent_dim=args.latent_dim)
    speaker_encoder = SpeakerEncoder(latent_dim=args.latent_dim)
    model_discover = DisCover(model, f0_encoder, speaker_encoder)
    model_discover.set_train()
    discriminator = Discriminator(latent_dim=args.latent_dim)
    cp_mi_net = CLUBSample(
        args.latent_dim, args.latent_dim, args.latent_dim, args.batch_size
    )
    cs_mi_net = CLUBSample(
        args.latent_dim, args.latent_dim, args.latent_dim, args.batch_size
    )
    (
        train_dataloader0, train_dataloader1, train_dataloader2,
        valid_dataloader, val_dataloader80,
    ) = prepare_data(args)

    optimizer = nn.Adam(
        model_discover.trainable_params(),
        learning_rate=args.lr,
        weight_decay=args.weight_decay,
    )
    optimizer_cp_mi = nn.Adam(cp_mi_net.trainable_params(), learning_rate=args.lr)
    optimizer_cs_mi = nn.Adam(cs_mi_net.trainable_params(), learning_rate=args.lr)
    optimizer_adv = nn.Adam(discriminator.trainable_params(), learning_rate=args.lr)

    first_train_net = FirstStageTrainOneStepCell(
        model_discover, optimizer, cp_mi_net, cs_mi_net, discriminator, args
    )
    second_train_net = SecondStageTrainOneStepCell(
        cp_mi_net, cs_mi_net, optimizer_cp_mi, optimizer_cs_mi,
        discriminator, optimizer_adv, args,
    )

    for epoch in range(args.max_epoch):
        running_loss = 0
        total = len(train_dataloader0)
        shs100k_best_mean_avg_precision, cover80_best_mean_avg_precision = 0, 0
        for (
            (data_q0, data_t0, label0, pitch0, speaker0, spk_label0),
            (data_q1, data_t1, label1, pitch1, speaker1, spk_label1),
            (data_q2, data_t2, label2, pitch2, speaker2, spk_label2),
        ) in tqdm(
            zip(train_dataloader0, train_dataloader1, train_dataloader2), total=total
        ):
            for flag in range(3):
                if flag == 0:
                    data_q, data_t, label = data_q0, data_t0, label0
                    pitch, speaker, spk_label = pitch0, speaker0, spk_label0
                elif flag == 1:
                    data_q, data_t, label = data_q1, data_t1, label1
                    pitch, speaker, spk_label = pitch1, speaker1, spk_label1
                else:
                    data_q, data_t, label = data_q2, data_t2, label2
                    pitch, speaker, spk_label = pitch2, speaker2, spk_label2
            (
                loss, feature4infer_q, biased_feature_q, pitch_emb, speaker_emb,
            ) = first_train_net(data_q, data_t, label, pitch, speaker, spk_label)
            first_train_net.set_train(False)
            for _ in range(args.inner_train_loop):
                second_train_net(
                    feature4infer_q, biased_feature_q, pitch_emb, speaker_emb
                )
                running_loss += loss
            first_train_net.set_train(True)
        print("loss =", running_loss / total)
        if args.dataset == "shs100k":
            cover80_mean_avg_precision = val_slow(
                model_discover, val_dataloader80, epoch,
                is_test=True, name="cover80", args=args,
            )
            shs100k_mean_avg_precision = val_slow(
                model_discover, valid_dataloader, epoch,
                is_test=True, name="shs100k", args=args,
            )
            print("")
            if shs100k_mean_avg_precision > shs100k_best_mean_avg_precision:
                shs100k_best_mean_avg_precision = shs100k_mean_avg_precision
                model_discover.save_best(args.notes)
                print("*****************SHS100K BEST*****************")
            print("")
            if cover80_mean_avg_precision > cover80_best_mean_avg_precision:
                cover80_best_mean_avg_precision = cover80_mean_avg_precision
                print("*****************COVER80 BEST*****************")
        else:
            raise NotImplementedError
        print(
            "shs100k_best_mean_avg_precision =",
            shs100k_best_mean_avg_precision,
            "cover80_best_mean_avg_precision =",
            cover80_best_mean_avg_precision,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="DisCover")
    parser.add_argument(
        "--model",
        type=str,
        default="CQTNet",
        help="model choice [CQTNet, CQTTPPNet]",
    )
    parser.add_argument("--load_model_path", type=str, default="", help="ckpt path")
    parser.add_argument("--batch_size", type=int, default=32, help="batch size")
    parser.add_argument(
        "--num_workers", type=int, default=2, help="how many workers for loading data"
    )
    parser.add_argument("--latent_dim", type=int, default=300, help="dim for infer")
    parser.add_argument(
        "--max_epoch", type=int, default=100, help="outer training epoch"
    )
    parser.add_argument(
        "--lr", type=float, default=0.0004, help="initial learning rate"
    )
    parser.add_argument(
        "--lr_decay",
        type=float,
        default=0.8,
        help="when val_loss increase, lr = lr*lr_decay",
    )
    parser.add_argument(
        "--weight_decay",
        type=float,
        default=1e-5,
        help="weight of regularization",
    )
    parser.add_argument(
        "--mi_weight",
        type=float,
        default=0.01,
        help="the weight of mutual information loss",
    )
    parser.add_argument(
        "--inner_train_loop", type=int, default=1, help="inner training epoch"
    )
    parser.add_argument("--dataset", type=str, default="shs100k", help="dataset choice")
    parser.add_argument("--notes", type=str, default="test", help="name of log")
    my_args = parser.parse_args()
    train(my_args)
