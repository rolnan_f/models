# MIT License
#
# Copyright (c) 2020 zhesong yu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
'''
Modified from: https://github.com/yzspku/CQTNet
'''

from collections import namedtuple
import random
import numpy as np
import mindspore.dataset as ds


def cqt_augmentation(data, mask_ratio=0.1):
    h, w = data.shape
    _h = int(mask_ratio * h)
    _w = int(mask_ratio * w)
    mask_h = np.random.choice([i for i in range(h)], _h)
    mask_w = np.random.choice([i for i in range(w)], _w)
    data[mask_h, :] = 0
    data[:, mask_w] = 0
    return data


def cut_data(data, out_length):
    if out_length is not None:
        if data.shape[0] > out_length:
            max_offset = data.shape[0] - out_length
            offset = np.random.randint(max_offset)
            data = data[offset : (out_length + offset), :]
        else:
            offset = out_length - data.shape[0]
            data = np.pad(data, ((0, offset), (0, 0)), "constant")
    if data.shape[0] < 200:
        offset = 200 - data.shape[0]
        data = np.pad(data, ((0, offset), (0, 0)), "constant")
    return data


def cut_pitch(data, out_length, offset=None):
    if out_length is not None:
        if data.shape[0] > out_length:
            data = data[offset : (out_length + offset)]
        else:
            offset = out_length - data.shape[0]
            data = np.pad(data, ((0, offset),), "constant")

    if data.shape[0] < 200:
        offset = 200 - data.shape[0]
        data = np.pad(data, ((0, offset),), "constant")
    return data


def cut_data2(data, out_length):
    if out_length is not None:
        st = (data.shape[1] - out_length) // 2
        ed = st + out_length
        return data[:, st:ed, :]
    return data


def cut_data3(data, out_length, offset):
    if out_length is not None:
        if data.shape[0] > out_length:
            max_offset = data.shape[0] - out_length
            if offset is None:
                offset = np.random.randint(max_offset)
            data = data[offset : (out_length + offset)]
        else:
            offset = out_length - data.shape[0]
            data = np.pad(data, ((0, offset), (0, 0)), "constant")
    if data.shape[0] < 200:
        offset = 200 - data.shape[0]
        data = np.pad(data, ((0, offset), (0, 0)), "constant")
    return data, offset


def cut_data_front(data, out_length):
    if out_length is not None:
        if data.shape[0] > out_length:
            offset = 0
            data = data[offset : (out_length + offset), :]
        else:
            offset = out_length - data.shape[0]
            data = np.pad(data, ((0, offset), (0, 0)), "constant")
    if data.shape[0] < 200:
        offset = 200 - data.shape[0]
        data = np.pad(data, ((0, offset), (0, 0)), "constant")
    return data


def shorter(feature, mean_size=10):
    length = feature.shape[0]
    new_f = np.zeros((int(length / mean_size)), dtype=np.float64)
    for i in range(int(length / mean_size)):
        new_f[i] = feature[i * mean_size : (i + 1) * mean_size].mean(axis=0)
    return new_f


class CQTsingle:
    def __init__(self, mode="train", out_length=None, spk_class_num=100):
        self.indir = "../SHS100K_cqt_npy/"
        self.mode = mode
        self.song_dict = {}
        self.pitch_dir = "../separated_pitch_shs100k/"
        self.speaker_dir = "../SHS100K_speaker_embedding_random/"
        self.spk_dict_dir = "spk_pseudo_label_dict_" + str(spk_class_num) + ".npy"
        self.spk_dict = np.load(self.spk_dict_dir, allow_pickle=True).item()
        self.covers80_id = np.load("covers80_id.npy", allow_pickle=True).item()
        if mode == "train":
            filepath = "./data/shs100k_train.txt"
        elif mode == "val":
            filepath = "./data/shs100k_valid.txt"
        elif mode == "test":
            filepath = "./data/shs100k_test.txt"
        elif mode == "songs80":
            self.indir = "../covers80_cqt_npy/"
            filepath = "./data/songs80_list.txt"

        with open(filepath, "r") as fp:
            self.file_list = [line.rstrip() for line in fp]
        self.out_length = out_length

    def __getitem__(self, index):
        transform_train = ds.transforms.Compose(
            [
                lambda x: x.T,
                lambda x: x.astype(np.float32) / (np.max(np.abs(x)) + 1e-6),
                lambda x: cut_data(x, self.out_length),
                lambda x: x.transpose(1, 0),
                lambda x: np.expand_dims(x, axis=0),
            ]
        )

        transform_test = ds.transforms.Compose(
            [
                lambda x: x.T,
                lambda x: x.astype(np.float32) / (np.max(np.abs(x)) + 1e-6),
                lambda x: cut_data_front(x, self.out_length),
                lambda x: x.transpose(1, 0),
                lambda x: np.expand_dims(x, axis=0),
            ]
        )

        filename = self.file_list[index].strip()
        set_id, version_id = filename.split(".")[0].split("_")
        set_id, version_id = int(set_id), int(version_id)
        in_path = self.indir + filename + ".npy"
        data = np.load(in_path)
        if self.mode == "train":
            data = transform_train(data)
        else:
            data = transform_test(data)
        return data, np.array(set_id, np.int32)

    def __len__(self):
        return len(self.file_list)


class CQTpair:
    def __init__(
        self,
        mode="train",
        out_length=None,
        conv1d=False,
        original_setting=False,
        use_spk_pseudo_label=True,
        spk_class_num=100,
    ):
        self.indir = "../SHS100K_cqt_npy/"
        self.mode = mode
        self.conv1d = conv1d
        self.song_dict = {}
        self.pitch_dir = "../separated_pitch_shs100k/"
        self.speaker_dir = "../SHS100K_speaker_embedding_random/"
        self.spk_dict_dir = "spk_pseudo_label_dict_" + str(spk_class_num) + ".npy"
        self.spk_dict = np.load(self.spk_dict_dir, allow_pickle=True).item()
        self.covers80_id = np.load("covers80_id.npy", allow_pickle=True).item()
        self.output_class = namedtuple('output', ['query', 'target', 'label', 'pitch', 'speaker', 'spk_label'])
        if mode == "train":
            filepath = "./data/shs100k_train.txt"
        elif mode == "val":
            filepath = "./data/shs100k_valid.txt"
        elif mode == "test":
            filepath = "./data/shs100k_test.txt"
            if original_setting:
                filepath = "../TPPNet/data/SHS100K-test.txt"
        elif mode == "songs80":
            self.indir = "../covers80_cqt_npy/"
            filepath = "./data/songs80_list.txt"

        print("CQTpair filepath =", filepath, "mode =", mode)

        with open(filepath, "r") as fp:
            self.file_list = [
                line.rstrip()
                for line in fp
                if line.rstrip() != "2914_0"
                and line.split("_")[0] not in self.covers80_id
            ]
        for filename in self.file_list:
            set_id, version_id = filename.split(".")[0].split("_")
            set_id, version_id = int(set_id), int(version_id)
            if set_id in self.song_dict:
                self.song_dict.get(set_id).append(version_id)
            else:
                self.song_dict[set_id] = [version_id]
        self.out_length = out_length


    def __getitem__(self, index):
        transform_test = ds.transforms.Compose(
            [
                lambda x: x.T,
                lambda x: x.astype(np.float32) / (np.max(np.abs(x)) + 1e-6),
                lambda x: cut_data_front(x, self.out_length),
                lambda x: x.transpose(1, 0),
                lambda x: np.expand_dims(x, axis=0),
            ]
        )

        filename = self.file_list[index].strip()
        set_id, version_id_q = filename.split(".")[0].split("_")
        set_id, version_id_q = int(set_id), int(version_id_q)
        in_path_q = self.indir + filename + ".npy"

        data_q = np.load(in_path_q)
        version_id_t = version_id_q
        try:
            song_list = self.song_dict[set_id]
        except KeyError:
            raise KeyError

        if len(song_list) > 1:
            while version_id_q == version_id_t:
                version_id_t = random.choice(song_list)

        in_path_t = self.indir + str(set_id) + "_" + str(version_id_t) + ".npy"
        data_t = np.load(in_path_t)
        if len(song_list) == 1:
            data_t = cqt_augmentation(data_t)

        if self.mode == "train":
            data_q, offset = self.transform_train(data_q, offset=None)
            pitch_path = self.pitch_dir + filename + ".npy"
            pitch = np.load(pitch_path)
            pitch = (pitch - pitch.mean(axis=-1)) / (pitch.std(axis=-1) + 1e-6)
            mean_size = 20
            length = pitch.shape[0]
            new_pitch = np.zeros((int(length / mean_size),), dtype=np.float32)
            for i in range(int(length / mean_size)):
                new_pitch[i] = pitch[i * mean_size : (i + 1) * mean_size].mean(axis=-1)
            new_pitch = cut_pitch(new_pitch, self.out_length, offset)
            new_pitch = np.pad(new_pitch, ((0, 400 - new_pitch.shape[0]),), "constant")
            data_t, _ = self.transform_train(data_t, offset=None)
        else:
            data_q = transform_test(data_q)
            data_t = transform_test(data_t)

        if self.mode == "train":
            speaker_emb_path = self.speaker_dir + filename + ".npy"
            speaker_emb_q = np.load(speaker_emb_path)
            speaker_label = int(self.spk_dict[filename])

        output = self.output_class(
            data_q, data_t, np.int32(set_id),
            new_pitch, speaker_emb_q, np.int32(speaker_label),
        )
        return output


    def __len__(self):
        return len(self.file_list)


    def transform_train(self, x, offset):
        x = x.T
        x = x.astype(np.float32) / (np.max(np.abs(x)) + 1e-6)
        x, _offset = cut_data3(x, self.out_length, offset)
        x = x.transpose(1, 0)
        x = np.expand_dims(x, axis=0)
        return x, _offset
