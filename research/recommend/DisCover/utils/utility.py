# MIT License
#
# Copyright (c) 2020 zhesong yu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
'''
Modified from: https://github.com/yzspku/CQTNet
'''
import numpy as np
from tqdm import tqdm


def norm(x):
    return x / (np.linalg.norm(x, axis=1, keepdims=True) + 1e-6)


def calc_mean_avg_precision(array2d, version, que_range=None, max_k=1e10):
    if que_range is not None:
        que_s, que_t = que_range[0], que_range[1]
        if que_s == 0:
            ref_s, ref_t = que_t, len(array2d)
        else:
            ref_s, ref_t = 0, que_s
    else:
        que_s, que_t, ref_s, ref_t = 0, len(array2d), 0, len(array2d)

    new_array2d = []
    for u, row in enumerate(array2d[que_s:que_t]):
        row = [
            (v + ref_s, col)
            for (v, col) in enumerate(row[ref_s:ref_t])
            if u + que_s != v + ref_s
        ]
        new_array2d.append(row)
    mean_avg_precision, top10, rank1 = 0, 0, 0

    for u, row in tqdm(enumerate(new_array2d), total=len(new_array2d)):
        row.sort(key=lambda x: x[1])
        per_top10, per_rank1, per_mean_avg_precision = 0, 0, 0
        version_cnt = 0.0
        u = u + que_s
        for k, (v, _) in enumerate(row):
            if version[u] == version[v]:
                if k < max_k:
                    version_cnt += 1
                    per_mean_avg_precision += version_cnt / (k + 1)
                if per_rank1 == 0:
                    per_rank1 = k + 1
                if k < 10:
                    per_top10 += 1
        per_mean_avg_precision /= 1 if version_cnt < 0.0001 else version_cnt
        mean_avg_precision += per_mean_avg_precision
        top10 += per_top10
        rank1 += per_rank1
    return (
        mean_avg_precision / float(que_t - que_s),
        top10 / float(que_t - que_s) / 10,
        rank1 / float(que_t - que_s),
    )
