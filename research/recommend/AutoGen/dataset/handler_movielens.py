import os
import time
import random
import json
from utils import to_hdf, from_hdf
import pandas as pd
import numpy as np

root = "/NAS2020/Workspaces/DMGroup/zhuchenxv/Huawei/dataset/Movielens"
data_path1 = os.path.join(root, 'raw')
user_feature_names = ["user_id", "gender", "age", "occupation", "zip_code"]
item_feature_names = ["item_id", "title", "genres"]
rating_feature_names = ["user_id", "item_id", "rating", "timestamp"]
user_excel = pd.read_csv(os.path.join(data_path1, 'users.dat'),
                         sep='::',
                         header=None,
                         names=user_feature_names)
item_excel = pd.read_csv(os.path.join(data_path1, 'movies.dat'),
                         sep='::',
                         header=None,
                         names=item_feature_names)
inter_excel = pd.read_csv(os.path.join(data_path1, 'ratings.dat'),
                          sep='::',
                          header=None,
                          names=rating_feature_names)
inter_excel = inter_excel[inter_excel['rating'] >= 4]
print(user_excel.shape)  # 6040, 5
print(item_excel.shape)  # 3883, 3
print(inter_excel.shape)  # 1000209, 4

# split inactive user

while True:
    filter_before_num = inter_excel.shape[0]
    current_user_num = 0
    current_user = []
    user_id_click_num = inter_excel['user_id'].value_counts(dropna=False)
    for user in user_id_click_num.keys():
        if user_id_click_num[user] >= 10:
            current_user.append(user)
            current_user_num += 1
    print("filter before user num:", len(user_id_click_num),
          "current_user_num:", current_user_num)
    inter_excel = inter_excel[(inter_excel['user_id'].isin(current_user))]
    print("current inter num:", inter_excel.shape[0])
    current_item = []
    current_item_num = 0
    item_id_click_num = inter_excel['item_id'].value_counts(dropna=False)
    for item in item_id_click_num.keys():
        if item_id_click_num[item] >= 10:
            current_item.append(item)
            current_item_num += 1
    print("filter before item num:", len(item_id_click_num),
          "current_item_num:", current_item_num)
    inter_excel = inter_excel[(inter_excel['item_id'].isin(current_item))]
    user_excel = user_excel[(user_excel['user_id'].isin(current_user))]
    item_excel = item_excel[(item_excel['item_id'].isin(current_item))]
    new_current_user = set(user_excel['user_id'])
    new_current_item = set(item_excel['item_id'])
    inter_excel = inter_excel[(inter_excel['item_id'].isin(new_current_item))]
    inter_excel = inter_excel[(inter_excel['user_id'].isin(new_current_user))]
    print("uesr num:", len(new_current_user))
    print("item num:", len(new_current_item))
    print("inter num:", inter_excel.shape[0])
    filter_after_num = inter_excel.shape[0]
    if filter_after_num == filter_before_num:
        break

user_excel = user_excel[(user_excel['user_id'].isin(current_user))]
item_excel = item_excel[(item_excel['item_id'].isin(current_item))]
print(user_excel.shape)  # 6040, 5
print(item_excel.shape)  # 3260, 3
print(inter_excel.shape)  # 998539,

item_excel['title'] = item_excel['title'].apply(lambda x: x[:-6])

inter_excel['hour'] = inter_excel['timestamp'].apply(
    lambda x: time.localtime(x)[3])
inter_excel['weekday'] = inter_excel['timestamp'].apply(
    lambda x: time.localtime(x)[6])


def category_i(x, i):
    if pd.isnull(x):
        return "NoneType"
    x_i = x.split('|')
    if i >= len(x_i):
        return "NoneType"
    return x_i[i]

item_excel['category_%d' % 0] = item_excel['genres'].apply(lambda x: category_i(x, 0))
item_excel['category_%d' % 1] = item_excel['genres'].apply(lambda x: category_i(x, 1))
item_excel['category_%d' % 2] = item_excel['genres'].apply(lambda x: category_i(x, 2))

item_excel.drop(columns=['genres'], inplace=True)

# feature map

feature_map_dict = dict()
for feature in [
        'user_id', 'gender', 'age', 'occupation', 'zip_code', 'item_id',
        'title', 'category', 'hour', 'weekday'
]:
    print("feature:", feature)
    if feature in ['user_id', 'gender', 'age', 'occupation', 'zip_code']:
        all_set = set(user_excel[feature].unique())
    elif feature in ['item_id', 'title']:
        all_set = set(item_excel[feature].unique())
    elif feature in ['hour', 'weekday']:
        all_set = set(inter_excel[feature].unique())
    elif feature in ['category']:
        set1 = set(item_excel[feature + '_0'].unique())
        set2 = set(item_excel[feature + '_1'].unique())
        set3 = set(item_excel[feature + '_2'].unique())
        all_set = set.union(set1, set2, set3)
    else:
        exit(-1)
    print("len:", len(all_set))
    tmp_feature_map = dict()
    num = 0
    for key in all_set:
        key_str = str(key)
        if key_str not in tmp_feature_map.keys():
            tmp_feature_map[key_str] = num
            num += 1
    feature_map_dict[feature] = tmp_feature_map
    print("feature length:", len(tmp_feature_map))


def fun_map(x, field_feature_map):
    x = str(x)
    return field_feature_map[x]


for feature in ['user_id', 'gender', 'age', 'occupation', 'zip_code']:
    user_excel[feature] = user_excel[feature].apply(
        lambda x: fun_map(x, feature_map_dict[feature]))

for feature in ['item_id', 'title']:
    item_excel[feature] = item_excel[feature].apply(
        lambda x: fun_map(x, feature_map_dict[feature]))

# for feature in ['category']:
for i in range(3):
    item_excel['category_%d' % i] = item_excel['category_%d' % i].apply(
        lambda x: fun_map(x, feature_map_dict['category']))

for feature in ['hour', 'weekday', 'user_id', 'item_id']:
    inter_excel[feature] = inter_excel[feature].apply(
        lambda x: fun_map(x, feature_map_dict[feature]))

# merge & generate new feature
inter_excel = inter_excel.merge(user_excel,
                                on=['user_id']).merge(item_excel,
                                                      on=['item_id'
                                                          ])  # 998539, 14
# 定义新的user field
merge_user_feature = [
    'user_id', 'hour', 'weekday', 'gender', 'age', 'occupation', 'zip_code'
]

# max_value = inter_excel.max()
# for i in range(len(merge_user_feature)):
#     for j in range(i + 1, len(merge_user_feature)):
#         new_field_name = merge_user_feature[i] + '-' + merge_user_feature[j]
#         print("new_field_name:", new_field_name)
#         if (((max_value[merge_user_feature[i]] + 1) *
#              (max_value[merge_user_feature[j]] + 1)) > 10000):
#             print("too large, not use")
#             continue
#         feat1 = merge_user_feature[i]
#         feat2 = merge_user_feature[j]
#         inter_excel[new_field_name] = inter_excel[[
#             merge_user_feature[i], merge_user_feature[j]
#         ]].apply(lambda x: str(x[feat1]) + '-' + str(x[feat2]),
#                  axis=1)
#         print("use")

# new feature feature_map

for feature in [
        'hour-weekday', 'hour-gender', 'hour-age', 'hour-occupation',
        'weekday-gender', 'weekday-age', 'weekday-occupation', 'gender-age',
        'gender-occupation', 'gender-zip_code', 'age-occupation'
]:
    print("feature:", feature)
    all_set = set(inter_excel[feature].unique())
    print("len:", len(all_set))
    tmp_feature_map = dict()
    num = 0
    for key in all_set:
        key_str = str(key)
        if key_str not in tmp_feature_map.keys():
            tmp_feature_map[key_str] = num
            num += 1
    feature_map_dict[feature] = tmp_feature_map
    print("feature length:", len(tmp_feature_map))

for feature in [
        'hour-weekday', 'hour-gender', 'hour-age', 'hour-occupation',
        'weekday-gender', 'weekday-age', 'weekday-occupation', 'gender-age',
        'gender-occupation', 'gender-zip_code', 'age-occupation'
]:
    inter_excel[feature] = inter_excel[feature].apply(
        lambda x: fun_map(x, feature_map_dict[feature]))
# 998539, 25
data_path2 = os.path.join(root, 'preprocess')
np.save(os.path.join(data_path2, 'feature_map_dict.npy'), feature_map_dict)

# split train/test user add negative sample

inter_excel_train_pos = inter_excel.groupby('user_id').apply(
    lambda x: x.sort_values('timestamp', ascending=True)[0:int(len(
        x) * 0.8)]).reset_index(drop=True)
inter_excel_test_pos = inter_excel.groupby(
    'user_id').apply(lambda x: x.sort_values('timestamp', ascending=True)[int(
        len(x) * 0.8):]).reset_index(drop=True)

print(inter_excel_train_pos.shape)  # 796389, 25
print(inter_excel_test_pos.shape)  # 202150, 25

user_click_item_list = inter_excel.groupby('user_id').apply(
    lambda x: list(x.reset_index(drop=True)['item_id']))
user_click_item_train_list = inter_excel_train_pos.groupby('user_id').apply(
    lambda x: list(x.reset_index(drop=True)['item_id']))
user_click_item_test_list = inter_excel_test_pos.groupby('user_id').apply(
    lambda x: list(x.reset_index(drop=True)['item_id']))
user_click_item_train_test = pd.concat([
    user_click_item_train_list, user_click_item_test_list, user_click_item_list
],
                                       axis=1)
user_click_item_train_test.rename(columns={
    0: 'train_item',
    1: 'test_item',
    2: 'all_click_item'
},
                                  inplace=True)

inter_excel_train_pos['rating'] = 1

print(user_click_item_list.shape)

train_neg_sample_ratio = 10


def generate_negative_sampple(click_item_list,):
    click_item_len = len(click_item_list)
    neg_num = int(click_item_len * 0.8) * train_neg_sample_ratio
    items = set(np.arange(0, 2810)) - set(click_item_list)
    if neg_num > len(items):
        neg = []
        while neg_num > 0:
            tmp_neg_num = min(neg_num, len(items))
            tmp_neg = random.sample(list(items), tmp_neg_num)
            neg_num -= tmp_neg_num
            neg += tmp_neg
    else:
        neg = random.sample(list(items), neg_num)
    return neg

user_negative_item_train_list = user_click_item_list.apply(generate_negative_sampple)

inter_excel_train_neg = np.repeat(inter_excel_train_pos.drop('timestamp',
                                                             axis=1).values,
                                  train_neg_sample_ratio,
                                  axis=0)
inter_excel_train_neg[:, 2] = 0  # rating
for i in range(inter_excel_train_pos.shape[0]):
    tmp_data = inter_excel_train_neg[i * train_neg_sample_ratio]
    tmp_user_id = tmp_data[0]
    inter_excel_train_neg[i * train_neg_sample_ratio:(i + 1) * train_neg_sample_ratio, 1] = \
        user_negative_item_train_list[tmp_user_id][:train_neg_sample_ratio]
    user_negative_item_train_list[tmp_user_id] = user_negative_item_train_list[
        tmp_user_id][train_neg_sample_ratio:]

inter_hdf_train_pos = inter_excel_train_pos.drop('timestamp', axis=1).values
inter_hdf_train_pos[:, 2] = 1.0
inter_hdf_train_neg = inter_excel_train_neg
inter_hdf_train_neg[:, 2] = 0.0

train_data = np.vstack(
    (inter_hdf_train_pos, inter_hdf_train_neg))  # 8760279, 24
train_data = pd.DataFrame(train_data)
train_data.columns = [
    'user_id', 'item_id', 'rating', 'hour', 'weekday', 'gender', 'age',
    'occupation', 'zip_code', 'title', 'category_0', 'category_1',
    'category_2', 'hour-weekday', 'hour-gender', 'hour-age', 'hour-occupation',
    'weekday-gender', 'weekday-age', 'weekday-occupation', 'gender-age',
    'gender-occupation', 'gender-zip_code', 'age-occupation'
]

train_data_new = train_data.sample(frac=1)  # shuffle

user_feature = [
    'user_id', 'gender', 'age', 'occupation', 'zip_code', 'hour', 'weekday',
    'hour-weekday', 'hour-gender', 'hour-age', 'hour-occupation',
    'weekday-gender', 'weekday-age', 'weekday-occupation', 'gender-age',
    'gender-occupation', 'gender-zip_code', 'age-occupation'
]  # 7+11=18
item_feature = ['item_id', 'title', 'category_0', 'category_1',
                'category_2']  # 22-10=12
label = ['rating']  # 1

data_path3 = os.path.join(root, 'final')
to_hdf(train_data_new[user_feature].values,
       os.path.join(data_path3, 'train_data.hdf5'),
       key='user')  # 8760279, 18
to_hdf(train_data_new[item_feature].values,
       os.path.join(data_path3, 'train_data.hdf5'),
       key='item')
to_hdf(train_data_new[label].values,
       os.path.join(data_path3, 'train_data.hdf5'),
       key='label')

train_data_new.to_csv(os.path.join(data_path2, 'train_data.csv'))

test_user_last_inter = inter_excel.groupby('user_id').apply(
    lambda x: x.sort_values('timestamp', ascending=True)[-1:]).reset_index(
        drop=True)  # 6040
test_user_last_inter = test_user_last_inter[user_feature]
to_hdf(test_user_last_inter.values,
       os.path.join(data_path3, 'test_all_user.hdf5'),
       key='data')

user_click_item_train_test.to_csv(
    os.path.join(data_path3, 'user_click_item.csv'))
to_hdf(item_excel.values,
       os.path.join(data_path3, 'test_all_item.hdf5'),
       key='data')

inter_excel_train_pos.to_csv(
    os.path.join(data_path2, 'inter_excel_train_pos.csv'))
inter_excel_test_pos.to_csv(
    os.path.join(data_path2, 'inter_excel_test_pos.csv'))
user_excel.to_csv(os.path.join(data_path2, 'user_excel.csv'))
item_excel.to_csv(os.path.join(data_path2, 'item_excel.csv'))
inter_excel.to_csv(os.path.join(data_path2, 'inter_excel.csv'))

# user 6040
all_user_num = user_excel.shape[0]
test_order = [i for i in range(all_user_num)]
random.shuffle(test_order)
validation_user_order = test_order[:int(all_user_num / 4)]
test_user_order = test_order[int(all_user_num / 4):]
validation_user_order.sort()
test_user_order.sort()

# len(test_order)
# len(validation_user_order)
# len(test_user_order)
np.save(os.path.join(data_path3, 'all_test_order.npy'), test_order)
np.save(os.path.join(data_path3, 'validation_user_order.npy'),
        validation_user_order)
np.save(os.path.join(data_path3, 'only_test_order.npy'), test_user_order)

user_feature = [
    'user_id', 'gender', 'age', 'occupation', 'zip_code', 'hour', 'weekday',
    'hour-weekday', 'hour-gender', 'hour-age', 'hour-occupation',
    'weekday-gender', 'weekday-age', 'weekday-occupation', 'gender-age',
    'gender-occupation', 'gender-zip_code', 'age-occupation'
]  # 7+11=18
item_feature = ['item_id', 'title', 'category']
user_feature_size = []
item_feature_size = []
for i in range(len(user_feature)):
    user_feature_size.append(len(feature_map_dict[user_feature[i]]))

for i in range(len(item_feature)):
    item_feature_size.append(len(feature_map_dict[item_feature[i]]))

print(user_feature_size)
print(item_feature_size)

# way1 sort by click len
root = "/NAS2020/Workspaces/DMGroup/zhuchenxv/Huawei/dataset/Movielens"
data_path3 = os.path.join(root, 'final')
validation_user_order = np.load(
    os.path.join(data_path3, 'validation_user_order.npy'))
only_test_user_order = np.load(os.path.join(data_path3, 'only_test_order.npy'))

USER_CLICK_ITEM = pd.read_csv(data_path3 + '/user_click_item.csv')
USER_CLICK_ITEM_TRAIN = USER_CLICK_ITEM['train_item'][only_test_user_order]

USER_CLICK_ITEM_TRAIN_LEN = USER_CLICK_ITEM_TRAIN.apply(
    lambda x: len(json.loads(x)))
USER_CLICK_ITEM_LEN_DICT = dict()
for i in range(len(USER_CLICK_ITEM_TRAIN_LEN)):
    USER_CLICK_ITEM_LEN_DICT[
        USER_CLICK_ITEM_TRAIN_LEN.index[i]] = USER_CLICK_ITEM_TRAIN_LEN.iloc[i]

# user_id ->  train_len
print(len(USER_CLICK_ITEM_LEN_DICT))
sorted_user_click_item_len = sorted(USER_CLICK_ITEM_LEN_DICT.items(),
                                    key=lambda x: x[1],
                                    reverse=True)
print(sorted_user_click_item_len[:10])
print(sorted_user_click_item_len[-10:])
sorted_user_click_item_len = np.array(sorted_user_click_item_len)
print(sorted_user_click_item_len[:10])
print(sorted_user_click_item_len[-10:])

final_split_test_user = []
user_num = 0
for i in range(10):
    tmp_user_list = []
    tmp_user_begin = int(len(only_test_user_order) * i / 10)
    tmp_user_end = int(len(only_test_user_order) * (i + 1) / 10)
    print(tmp_user_begin, tmp_user_end)
    tmp_user_list = sorted_user_click_item_len[tmp_user_begin:tmp_user_end, 0]
    # print(tmp_user_list[0])
    print(len(tmp_user_list))
    final_split_test_user.append(tmp_user_list)
    user_num += len(tmp_user_list)
print("user num:", user_num)
np.save(os.path.join(data_path3, 'split_test_user_way1.npy'),
        final_split_test_user)

# way3 sort by avg grade
root = "/NAS2020/Workspaces/DMGroup/zhuchenxv/Huawei/dataset/Movielens"
data_path1 = os.path.join(root, 'raw')
data_path2 = os.path.join(root, 'preprocess')
data_path3 = os.path.join(root, 'final')
rating_feature_names = ["user_id", "item_id", "rating", "timestamp"]
inter_excel = pd.read_csv(os.path.join(data_path1, 'ratings.dat'),
                          sep='::',
                          header=None,
                          names=rating_feature_names)
user_avg_grade = inter_excel.groupby('user_id')['rating'].mean()
feature_map_dict = np.load(data_path2 + '/feature_map_dict.npy',
                           allow_pickle=True).item()
user_feature_map = feature_map_dict['user_id']
final_user_avg_grade_dict = dict()
for user in user_avg_grade.index:
    if user in user_feature_map.keys():
        final_user_avg_grade_dict[
            user_feature_map[user]] = user_avg_grade[user]
    elif str(user) in user_feature_map.keys():
        final_user_avg_grade_dict[user_feature_map[str(
            user)]] = user_avg_grade[user]
    else:
        print("user:", user, "not exist in feature map")
np.save(data_path3 + '/user_avg_grade.npy', final_user_avg_grade_dict)

USER_AVG_GRADE = np.load(data_path3 + '/user_avg_grade.npy',
                         allow_pickle=True).item()
only_test_user_order = np.load(os.path.join(data_path3, 'only_test_order.npy'))
USER_CLICK_ITEM = pd.read_csv(data_path3 + '/user_click_item.csv')
USER_CLICK_ITEM_TRAIN = USER_CLICK_ITEM['train_item'][only_test_user_order]
test_all_user_feature_context = from_hdf(data_path3 + '/test_all_user.hdf5',
                                         'data')
test_all_user_feature_context = test_all_user_feature_context[
    only_test_user_order]

USER_AVG_GRADE_SPLIT_VAL = dict()
print(USER_CLICK_ITEM_TRAIN.index)
for user_i in range(len(USER_CLICK_ITEM_TRAIN)):
    tmp_user_id = USER_CLICK_ITEM_TRAIN.index[user_i]
    tmp_user_id2 = test_all_user_feature_context[user_i][0]
    assert tmp_user_id == tmp_user_id2
    tmp_score = USER_AVG_GRADE[tmp_user_id]
    USER_AVG_GRADE_SPLIT_VAL[tmp_user_id] = tmp_score
    if user_i <= 10:
        print(tmp_user_id, tmp_score)

print(len(USER_AVG_GRADE_SPLIT_VAL))
sorted_user_score = sorted(USER_AVG_GRADE_SPLIT_VAL.items(),
                           key=lambda x: x[1],
                           reverse=True)
print(sorted_user_score[:10])
print(sorted_user_score[-10:])
sorted_user_score = np.array(sorted_user_score)
print(sorted_user_score[:10])
print(sorted_user_score[-10:])
final_split_test_user = []
user_num = 0
for i in range(10):
    print("part:", i)
    tmp_user_list = []
    tmp_user_begin = int(len(only_test_user_order) * i / 10)
    tmp_user_end = int(len(only_test_user_order) * (i + 1) / 10)
    print(tmp_user_begin, tmp_user_end)
    tmp_user_list = sorted_user_score[tmp_user_begin:tmp_user_end, 0]
    # print(tmp_user_list)
    print(sorted_user_score[tmp_user_begin:tmp_user_begin + 5])
    print(sorted_user_score[tmp_user_end - 5:tmp_user_end])
    print(len(tmp_user_list))
    final_split_test_user.append(tmp_user_list)
    user_num += len(tmp_user_list)
print("user num:", user_num)
np.save(os.path.join(data_path3, 'split_test_user_way3.npy'),
        final_split_test_user)
