import os
import random
import json
from utils import from_hdf
import pandas as pd
import numpy as np


class DatasetHelper:

    def __init__(self, dataset, kwargs):
        self.dataset = dataset
        self.kwargs = kwargs

    def __iter__(self):
        for x in self.dataset.__iter__(**self.kwargs):
            yield x

    @property
    def batch_size(self):
        return self.kwargs['batch_size']

    @property
    def gen_type(self):
        return self.kwargs['gen_type']

    @property
    def val_ratio(self):
        return self.kwargs['val_ratio']


class Movielens:

    def __init__(self, neg_sample_num=3, importance_way=3):
        assert importance_way == 3
        self.train_size = 796389 * (neg_sample_num + 1)  # 8760279
        self.all_user_num = 6040
        self.all_item_num = 3260
        self.neg_sample_num = neg_sample_num
        self.BASE_PATH = '/NAS2020/Workspaces/DMGroup/zcx/Huawei/dataset/Movielens/final/'
        # self.train_path = os.path.join(self.BASE_PATH, 'train_data.hdf5')
        self.train_path = '/NAS2020/Workspaces/DMGroup/zcx/Huawei/dataset/Movielens/preprocess/' \
                          'inter_excel_train_pos.csv'
        self.train_data_pos = None
        self.USER_CONTEXT = None
        self.test_path = os.path.join(self.BASE_PATH, 'test_all_user.hdf5')
        self.item_path = os.path.join(self.BASE_PATH, 'test_all_item.hdf5')
        self.click_item_path = os.path.join(self.BASE_PATH,
                                            'user_click_item.csv')
        self.user_feature = [
            'user_id', 'gender', 'age', 'occupation', 'zip_code', 'hour',
            'weekday', 'hour-weekday', 'hour-gender', 'hour-age',
            'hour-occupation', 'weekday-gender', 'weekday-age',
            'weekday-occupation', 'gender-age', 'gender-occupation',
            'gender-zip_code', 'age-occupation'
        ]  # 7+11=18
        self.item_feature = [
            'item_id', 'title', 'category_0', 'category_1', 'category_2'
        ]  # 3
        # label = ['rating']
        user_feature_size = [
            6040, 2, 7, 21, 3439, 24, 7, 168, 48, 168, 496, 14, 49, 147, 14,
            42, 4108, 134
        ]
        item_feature_size = [3260, 3221, 19]
        user_pre_sum = []
        user_num = 0
        for i in range(len(user_feature_size)):
            user_pre_sum.append(user_num)
            user_num += user_feature_size[i]
        item_pre_sum = []
        item_num = 0
        for i in range(len(item_feature_size)):
            item_pre_sum.append(item_num)
            item_num += item_feature_size[i]
        self.user_pre_sum = user_pre_sum
        self.item_pre_sum = item_pre_sum

        self.user_field_num = 18
        self.item_field_num = 2
        self.item_multi_field_num = 1
        self.user_feature_num = sum(user_feature_size)
        self.item_feature_num = sum(item_feature_size)
        self.all_item_one_hot, self.all_item_multi_hot = None, None
        self.USER_CLICK_ITEM = pd.read_csv(self.click_item_path)
        self.item_excel = pd.read_csv(
            '/NAS2020/Workspaces/DMGroup/zcx/Huawei/dataset/Movielens/preprocess/item_excel.csv'
        )
        self.validation_user_order = np.load(
            os.path.join(self.BASE_PATH, 'validation_user_order.npy'))
        self.only_test_user_order = np.load(
            os.path.join(self.BASE_PATH, 'only_test_order.npy'))
        self.test_split_order = np.load(os.path.join(
            self.BASE_PATH, 'split_test_user_way%d.npy' % importance_way),
                                        allow_pickle=True)

    def get_all_item(self):
        if self.all_item_one_hot is None or self.all_item_multi_hot is None:
            item = from_hdf(self.item_path, 'data')
            item_cate_len = np.ones(shape=(item.shape[0], 1),
                                    dtype=np.int32) * 3
            item = np.hstack((item, item_cate_len))
            item_one_hot = item[:, :self.
                                item_field_num] + self.item_pre_sum[:self.
                                                                    item_field_num]
            item_multi_hot = item[:, self.item_field_num:]
            item_multi_hot[:, :-1] += (item_multi_hot[:, :-1] >=
                                       0) * self.item_pre_sum[-1]
            self.all_item_one_hot = item_one_hot
            self.all_item_multi_hot = item_multi_hot
            return item_one_hot, item_multi_hot
        return self.all_item_one_hot, self.all_item_multi_hot

    def __iter__(self,
                 batch_size=512,
                 part='train',
                 shuffle=True,
                 split_user=0):
        if part == 'train':
            if self.train_data_pos is None:
                train_data_pos = pd.read_csv(self.train_path)
                train_data_pos = train_data_pos[self.user_feature +
                                                ['item_id', 'rating']]
                self.train_data_pos = train_data_pos

            train_data_pos = self.train_data_pos
            USER_CLICK_ITEM_TRAIN = self.USER_CLICK_ITEM['train_item']
            # func1 = lambda x: self.generate_negative_sampple(x)
            # user_negative_item_train_list = USER_CLICK_ITEM_TRAIN.apply(func1)
            user_negative_item_train_list = USER_CLICK_ITEM_TRAIN.apply(self.generate_negative_sampple)
            train_data_neg = np.repeat(
                train_data_pos[self.user_feature +
                               ['item_id', 'rating']].values,
                self.neg_sample_num,
                axis=0)
            train_data_neg[:, -1] = 0  # rating
            print("generate negative sample")
            for i in range(train_data_pos.shape[0]):
                tmp_data = train_data_neg[i * self.neg_sample_num]
                tmp_user_id = tmp_data[0]
                train_data_neg[i * self.neg_sample_num:(i + 1) * self.neg_sample_num, -2] = \
                    user_negative_item_train_list[tmp_user_id][:self.neg_sample_num]
                user_negative_item_train_list[
                    tmp_user_id] = user_negative_item_train_list[tmp_user_id][
                        self.neg_sample_num:]
            train_data_pos = train_data_pos[self.user_feature +
                                            ['item_id', 'rating']].values
            train_data_pos[:, -1] = 1
            train_data = np.vstack(
                (train_data_pos, train_data_neg))  # 8760279, 24

            train_data = pd.DataFrame(train_data)
            train_data.columns = self.user_feature + ['item_id', 'rating']

            train_data = train_data.merge(self.item_excel, on=['item_id'])

            train_data = train_data.sample(frac=1)
            self.train_data = train_data

            USER = self.train_data[self.user_feature].values
            ITEM = self.train_data[self.item_feature].values
            Y = self.train_data[['rating']].values

            num_of_batches = int(np.ceil(Y.shape[0] * 1.0 / batch_size))
            sample_index = np.arange(Y.shape[0])
            if shuffle:
                np.random.shuffle(sample_index)
            for i in range(num_of_batches):
                batch_index = sample_index[batch_size * i:batch_size * (i + 1)]
                if batch_index.shape[0] < 1:
                    continue
                user = USER[batch_index] + self.user_pre_sum
                item = ITEM[batch_index]
                item_cate_len = np.ones(shape=(user.shape[0], 1),
                                        dtype=np.int32) * 3
                item = np.hstack((item, item_cate_len))
                item_one_hot = item[:, :self.
                                    item_field_num] + self.item_pre_sum[:self.
                                                                        item_field_num]
                item_multi_hot = item[:, self.item_field_num:]
                item_multi_hot[:, :-1] += (item_multi_hot[:, :-1] >=
                                           0) * self.item_pre_sum[-1]
                y = Y[batch_index][:, 0]
                yield user, item_one_hot, item_multi_hot, y
        elif part == 'validation':
            if self.USER_CONTEXT is None:
                path = self.test_path
                self.USER_CONTEXT = from_hdf(path, 'data')
            USER_CLICK_ITEM_TRAIN = self.USER_CLICK_ITEM['train_item']
            USER_CLICK_ITEM_TEST = self.USER_CLICK_ITEM['test_item']
            num_of_batches = int(
                np.ceil(self.validation_user_order.shape[0] * 1.0 /
                        batch_size))
            sample_index = self.validation_user_order
            for i in range(num_of_batches):
                batch_index = sample_index[batch_size * i:batch_size * (i + 1)]
                if batch_index.shape[0] < 1:
                    continue
                user_context = self.USER_CONTEXT[
                    batch_index] + self.user_pre_sum
                user_click_item_train = USER_CLICK_ITEM_TRAIN[batch_index]
                user_click_item_test = USER_CLICK_ITEM_TEST[batch_index]
                yield user_context, user_click_item_train, user_click_item_test
        elif part == 'new_test':
            if self.USER_CONTEXT is None:
                path = self.test_path
                self.USER_CONTEXT = from_hdf(path, 'data')
            USER_CLICK_ITEM_TRAIN = self.USER_CLICK_ITEM['train_item']
            USER_CLICK_ITEM_TEST = self.USER_CLICK_ITEM['test_item']
            num_of_batches = int(
                np.ceil(self.only_test_user_order.shape[0] * 1.0 / batch_size))
            sample_index = self.only_test_user_order
            # print(sample_index)
            for i in range(num_of_batches):
                batch_index = sample_index[batch_size * i:batch_size * (i + 1)]
                if batch_index.shape[0] < 1:
                    continue
                user_context = self.USER_CONTEXT[
                    batch_index] + self.user_pre_sum
                user_click_item_train = USER_CLICK_ITEM_TRAIN[batch_index]
                user_click_item_test = USER_CLICK_ITEM_TEST[batch_index]
                yield user_context, user_click_item_train, user_click_item_test
        elif part == 'new_test_split':
            if self.USER_CONTEXT is None:
                path = self.test_path
                self.USER_CONTEXT = from_hdf(path, 'data')
            USER_CLICK_ITEM_TRAIN = self.USER_CLICK_ITEM['train_item']
            USER_CLICK_ITEM_TEST = self.USER_CLICK_ITEM['test_item']
            num_of_batches = int(
                np.ceil(self.only_test_user_order.shape[0] * 1.0 / batch_size))
            sample_index_test = self.test_split_order[split_user]
            sample_index_test = np.array(list(map(int, sample_index_test)))

            for i in range(num_of_batches):
                batch_index = sample_index_test[batch_size * i:batch_size *
                                                (i + 1)]
                if batch_index.shape[0] < 1:
                    continue
                user_context = self.USER_CONTEXT[
                    batch_index] + self.user_pre_sum
                user_click_item_train = USER_CLICK_ITEM_TRAIN[batch_index]
                user_click_item_test = USER_CLICK_ITEM_TEST[batch_index]
                yield user_context, user_click_item_train, user_click_item_test
        else:
            raise 'Invalid part: {}'.format(part)

    def batch_generator(self, kwargs):
        return DatasetHelper(self, kwargs)

    def generate_negative_sampple(
        self,
        click_item_list,
    ):
        click_item_list = json.loads(click_item_list)
        click_item_len = len(click_item_list)
        # neg_num = int(click_item_len * 0.8) * self.neg_sample_num
        neg_num = int(click_item_len) * self.neg_sample_num
        items = set(np.arange(0, self.all_item_num)) - set(click_item_list)
        if neg_num > len(items):
            neg = []
            while neg_num > 0:
                tmp_neg_num = min(neg_num, len(items))
                tmp_neg = random.sample(list(items), tmp_neg_num)
                neg_num -= tmp_neg_num
                neg += tmp_neg
        else:
            neg = random.sample(list(items), neg_num)
        return neg


if __name__ == "__main__":
    a = Movielens()
