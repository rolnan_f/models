import os
import numpy as np
from mindspore import Tensor
from mindspore import save_checkpoint
import tensorflow as tf
from mindspore_tf_models import DSSM_multi_input_once_for_all
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

params_mapping = {
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_1/moving_variance": "user_bn1_var",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_1/moving_mean": "user_bn1_mean",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_1/gamma": "user_bn1_gamma",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_1/beta": "user_bn1_beta",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_w_2": "user_dnn_w_2",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_w_0": "user_dnn_w_0",
    "item_v": "item_v",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_0/moving_variance": "item_bn0_var",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_0/moving_mean": "item_bn0_mean",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_0/gamma": "item_bn0_gamma",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_0/beta": "item_bn0_beta",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_w_1": "item_dnn_w_1",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_b_0": "user_dnn_b_0",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_b_0": "item_dnn_b_0",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_1/moving_variance": "item_bn1_var",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_1/moving_mean": "item_bn1_mean",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_1/gamma": "item_bn1_gamma",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_mlp_bn_1/beta": "item_bn1_beta",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_w_2": "item_dnn_w_2",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_b_1": "user_dnn_b_1",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_b_1": "item_dnn_b_1",
    "user_v": "user_v",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_b_2": "user_dnn_b_2",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_b_2": "item_dnn_b_2",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_0/moving_variance": "user_bn0_var",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_0/moving_mean": "user_bn0_mean",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_0/gamma": "user_bn0_gamma",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_mlp_bn_0/beta": "user_bn0_beta",
    "DNN_multi_input_unshare_user_dnn_/user_dnn_w_1": "user_dnn_w_1",
    "DNN_multi_input_unshare_item_dnn_/item_dnn_w_0": "item_dnn_w_0",
}


def tensorflow_param(ckpt_path):
    """Get TensorFlow parameter and shape"""
    tf_param = {}
    reader = tf.train.load_checkpoint(ckpt_path)
    for name in reader.get_variable_to_shape_map():
        try:
            print(name, reader.get_tensor(name).shape)
            tf_param[name] = reader.get_tensor(name)
        except AttributeError as e:
            print(e)
    return tf_param


def mindspore_params(net):
    """Get MindSpore parameter and shape"""
    ms_param = {}
    for param in net.get_parameters():
        name = param.name
        value = param.data.asnumpy()
        print(name, value.shape)
        ms_param[name] = value
    return ms_param


def tensorflow2mindspore(tf_ckpt_dir, param_mapping_dict, ms_ckpt_path):
    """convert tensorflow ckpt to mindspore ckpt"""
    reader = tf.train.load_checkpoint(tf_ckpt_dir)
    new_params_list = []
    for name in param_mapping_dict:
        param_dict = {}
        parameter = reader.get_tensor(name)
        param_dict['name'] = param_mapping_dict[name]
        param_dict['data'] = Tensor(parameter)
        new_params_list.append(param_dict)
    save_checkpoint(new_params_list, ms_ckpt_path)
    print("new path: ", ms_ckpt_path)


def mean_relative_error(y_expect, y_pred):
    """mean relative error"""
    if y_expect.dtype == np.bool:
        y_expect = y_expect.astype(np.int32)
        y_pred = y_pred.astype(np.int32)

    rerror = np.abs(y_expect - y_pred)/np.maximum(np.abs(y_expect), np.abs(y_pred))
    rerror = rerror[~np.isnan(rerror)]
    rerror = rerror[~np.isinf(rerror)]
    relative_error_out = np.mean(rerror)
    return relative_error_out

if __name__ == "__main__":
    print("zcxzcx")
    tf_model_path = './tf_checkpoints'
    ms_model_path = './ms_checkpoints/tf2mindspore1.ckpt'
    tf_params = tensorflow_param(tf_model_path)
    print("*" * 50)
    user_field_num = 28
    item_field_num = 2
    user_feature_num = 14928
    item_feature_num = 6500
    network = DSSM_multi_input_once_for_all(user_field_num=user_field_num, item_field_num=item_field_num,
                                            user_feature_num=user_feature_num, item_feature_num=item_feature_num,
                                            embedding_size=40,
                                            user_dnn_hidden_unit=[512, 256, 128], item_dnn_hidden_unit=[512, 256, 128])
    ms_params = mindspore_params(network)
    tensorflow2mindspore(tf_model_path, params_mapping, ms_model_path)
