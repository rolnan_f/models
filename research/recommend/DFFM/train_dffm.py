# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import time
import os
import argparse
import pickle as pkl
import numpy as np
import pandas as pd
import mindspore.nn as nn
import mindspore.dataset as ds
from mindspore import context

from src.dffm import DFFM, Ctr_Loss, CustomWithLossCell, TrainOneStepCell
from sklearn.metrics import roc_auc_score, log_loss


def get_aliccp_ctr_df(path, used_cols, seq_cols=None, topk=10, nrows=None):
    data_frame = pd.read_csv(path, header=0, usecols=used_cols, nrows=nrows)
    print(data_frame.shape)
    data_dict = {}
    for col in used_cols:
        if col in seq_cols:
            seq_list = data_frame[col].tolist()
            data_dict[col] = np.array([seq.split("|") + [0] * (topk - len(seq.split("|"))) for seq in seq_list],
                                      dtype=int)
        else:
            data_dict[col] = np.array(data_frame[col], dtype=int)
    return data_dict


def load_pickle(path):
    with open(path, 'rb') as f:
        return pkl.load(f)


def metric(y, p, name='ctr'):
    y = np.array(y)
    p = np.array(p)
    auc = roc_auc_score(y, p)

    orilen = len(p)
    ind = np.where((p > 0) & (p < 1))[0]
    y = y[ind]
    p = p[ind]
    afterlen = len(p)

    ll = log_loss(y, p) * afterlen / orilen
    q = y.mean()

    print('avg %s on p: %g\teval auc: %g\tlog loss: %g' % (name, q, auc, ll))
    return auc

def boolean_string(s):
    if s not in {'False', 'True'}:
        raise ValueError('Not a valid boolean string')
    return s == 'True'


def parse_args():
    """parse_args"""
    parser = argparse.ArgumentParser(description='Recommend System with DIEN')
    # path
    parser.add_argument('--dataset_file_path', type=str, default='/home/ma-user/work/DIEN/dataset/aliccp',
                        help='dataset files path')

    # train args
    parser.add_argument('--epoch_size', type=int, default=1)
    parser.add_argument('--batch_size', type=int, default=1024)
    parser.add_argument('--max_len', type=int, default=10)
    parser.add_argument('--base_lr', type=float, default=0.001, help='Base learning rate')
    parser.add_argument('--domain_col', type=str, default='301')
    parser.add_argument('--add_dffi', type=boolean_string, default=True)
    parser.add_argument('--add_dfub', type=boolean_string, default=True)
    parser.add_argument('--use_reshape_value', type=boolean_string, default=True)
    parser.add_argument('--hidden_unit', type=int, default=128)
    parser.add_argument('--hidden_depth', type=int, default=5)
    parser.add_argument('--embedding_dim', type=int, default=16)
    parser.add_argument('--l2_reg', type=float, default=1e-5)

    # device args
    parser.add_argument('--device_target', type=str, default='GPU', help='Device target')
    parser.add_argument('--device_id', type=int, default=0)

    args = parser.parse_args()
    return args


# hyper-parameter
args_opt = parse_args()

labels = ['click']
sparse_features = ['101', '205', '121', '122', '124', '125', '126', '127', '128', '129', '206', '207', '216', '301']
var_features = ['109_14', '110_14', '127_14', '150_14']
domain_feature = '301'
domain_index = sparse_features.index(domain_feature)
EMBEDDING_DIM = args_opt.embedding_dim
epoch_size = args_opt.epoch_size
base_lr = args_opt.base_lr
batch_size = args_opt.batch_size
max_len = args_opt.max_len
use_reshape_value = args_opt.use_reshape_value
hidden_unit = args_opt.hidden_unit
hidden_depth = args_opt.hidden_depth
l2_reg = args_opt.l2_reg

def get_data(data):
    for index in range(data[sparse_features[0]].size):
        sparse_value_list = []
        for feat in sparse_features:
            value = data[feat][index]
            sparse_value_list.append(value)
        sparse_value_array = np.array(sparse_value_list).astype(np.int32)
        value_list = [sparse_value_array]
        for feat in var_features:
            value = data[feat][index]
            value_list.append(value)
        value_array = np.concatenate(value_list, axis=0)
        label = np.array(data[labels[0]][index]).astype(np.float32)
        yield value_array, label


def create_dataset(data, batch_size):
    """生成数据集"""
    dataset = ds.GeneratorDataset(get_data(data), column_names=["features", "label"])
    dataset = dataset.batch(batch_size)
    return dataset


def train(train_set, test_set, sparse_features, var_features):
    # size of embedding matrices
    data_max = {'101': 265945, '109_14': 9912, '110_14': 553428, '127_14': 187292, '150_14': 80199,
                '121': 97, '122': 13, '124': 2, '125': 7, '126': 3, '127': 3, '128': 2, '129': 4, '205': 1010133,
                '206': 7362, '207': 351207, '216': 132501, '301': 3}

    history_feature_list = ["206", "207", "216"]
    history_seq_list = ['109_14', '110_14', '127_14']
    domain_column_list = [domain_feature]
    print("use_reshape_value:", use_reshape_value,
          "hidden_unit:", hidden_unit, "hidden_depth:", hidden_depth, "batch_size:", batch_size,
          "l2_reg:", l2_reg)
    model = DFFM(sparse_features, var_features, domain_index, data_max, embedding_size=EMBEDDING_DIM,
                 domain_feature_list=domain_column_list, history_feature_list=history_feature_list,
                 history_seq_list=history_seq_list, use_reshape_value=use_reshape_value, hidden_unit=hidden_unit,
                 hidden_depth=hidden_depth, l2_reg=l2_reg)

    # loss function
    loss_fn = Ctr_Loss()
    net_with_criterion = CustomWithLossCell(model, loss_fn)
    optimizer = nn.Adam(model.trainable_params(), learning_rate=base_lr)
    train_net = TrainOneStepCell(net_with_criterion, optimizer)

    print("trainable_params:", len(model.trainable_params()))
    print("-" * 50)
    for para in model.trainable_params():
        print(para)
    print("-" * 50)

    best_Auc = 0.0
    for epoch in range(epoch_size):
        train_dataset = create_dataset(train_set, batch_size)
        time_start = time.time()
        print("time_start:", time_start)
        step = 0
        step_size = round(len(train_set[sparse_features[0]]) / batch_size)
        for d_train in train_dataset.create_dict_iterator():
            step += 1
            _ = train_net(d_train["features"], d_train["label"])
            if step % 200 == 0:
                print('Epoch %d Global_step %d All_step %d \t' % (epoch, step, step_size))
                test_dataset = create_dataset(test_set, batch_size)
                test_labels, test_preds = [], []
                for d_test in test_dataset.create_dict_iterator():
                    _, y_hat, _ = model(d_test['features'])
                    test_labels.extend(list(d_test['label'].asnumpy()))
                    test_preds.extend(list(y_hat.asnumpy()))
                test_Auc = metric(np.array(test_labels), np.array(test_preds))
                if test_Auc > best_Auc:
                    best_Auc = test_Auc
                print('current test auc', test_Auc, 'best auc', best_Auc)
        # test
        print('Epoch %d Global_step %d All_step %d \t' % (epoch, step, step_size))
        test_dataset = create_dataset(test_set, batch_size)
        test_labels, test_preds = [], []
        for d_test in test_dataset.create_dict_iterator():
            _, y_hat, _ = model(d_test['features'])
            test_labels.extend(list(d_test['label'].asnumpy()))
            test_preds.extend(list(y_hat.asnumpy()))
        test_Auc = metric(np.array(test_labels), np.array(test_preds))
        if test_Auc > best_Auc:
            best_Auc = test_Auc
        print('current test auc', test_Auc, 'best auc', best_Auc)

        time_end = time.time()
        print('epoch_time:', time_end - time_start)


def not_modelarts():
    """not_modelarts"""
    dataset_file_path = args_opt.dataset_file_path

    device_id = args_opt.device_id
    device_target = args_opt.device_target

    train_set = get_aliccp_ctr_df(os.path.join(dataset_file_path, 'ctr_cvr.train'),
                                  labels + sparse_features + var_features, seq_cols=var_features)
    print('load train finish')
    test_set = get_aliccp_ctr_df(os.path.join(dataset_file_path, 'ctr_cvr.test'),
                                 labels + sparse_features + var_features, seq_cols=var_features)
    print('load test finish')

    context.set_context(mode=context.PYNATIVE_MODE, device_target=device_target, device_id=device_id)
    train(train_set, test_set, sparse_features, var_features)


def main():
    print("execution not_modelarts Method")
    not_modelarts()


if __name__ == '__main__':
    main()
