# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import os
from datetime import datetime

import yaml
import mindspore as ms
import mindspore.ops as ops
from mindspore.nn.optim import Adam
from mindspore import nn, ParameterTuple
from mindspore.nn.learning_rate_schedule import LearningRateSchedule
from mindspore.train.callback import ModelCheckpoint, CheckpointConfig

from .callback import EvalCallBack, LossCallBack, TimeMonitor
from .models.loss import LFLossClass, SALossClass
from .models.MDLF import MDLF, BasicDLF
from .models.Hierarch import Hierarch
from .models.DeepHit import DeepHit
from .models.DWPP import DWPP
from .models.ADM import ADM


class StepLR(LearningRateSchedule):
    def __init__(self, learning_rate, step_size, gamma):
        super(StepLR, self).__init__()
        self.learning_rate = ops.cast(learning_rate, ms.float32)
        self.step_size = step_size
        self.gamma = gamma
        self.pow = ops.Pow()

    def construct(self, global_step):
        return self.learning_rate * self.pow(self.gamma, global_step // self.step_size)


class TrainStepWrap(nn.Cell):
    """
    TrainStepWrap definition
    """

    def __init__(self, network, step_per_epoch, lr=5e-8, eps=1e-8, loss_scale=1, beta1=0.9, beta2=0.999, amsgrad=False):
        super(TrainStepWrap, self).__init__(auto_prefix=False)
        self.network = network
        self.network.set_grad()
        self.network.set_train()
        self.weights = ParameterTuple(network.trainable_params())
        self.optimizer = Adam(self.weights, learning_rate=StepLR(lr, 5 * step_per_epoch, 0.9), eps=eps, beta1=beta1,
                              beta2=beta2, loss_scale=loss_scale, use_amsgrad=amsgrad)

    def construct(self, batch_ids, pos_lbl, score_lbl):
        weights = self.weights
        grads, (loss_item,) = ops.grad(self.network, grad_position=None, weights=weights, has_aux=True)(batch_ids,
                                                                                                        pos_lbl,
                                                                                                        score_lbl)
        self.optimizer(grads)
        return loss_item


class EvalNet(nn.Cell):
    def __init__(self, network):
        super(EvalNet, self).__init__(auto_prefix=False)
        self.network = network

    def construct(self, batch_idx, pos_lbl, score_lbl):
        pdf, cdf, winning_rate = self.network(batch_idx)
        return [pdf, cdf, winning_rate, pos_lbl, score_lbl]


class ModelBuilder:
    """
    Model builder for MDLF and baselines.
    """

    def __init__(self, config, step_per_epoch, dump_config=True):
        self.config = config
        self.step_per_epoch = step_per_epoch
        self.run_id = datetime.now().strftime(r'%m%d_%H%M%S')
        print('Run ID: ', self.run_id)
        self.output_dir = os.path.join(self.config.output_path, self.config.algorithm, self.run_id)
        os.makedirs(self.output_dir)
        if dump_config:
            with open(os.path.join(self.output_dir, 'config.yaml'), 'w') as f:
                yaml.safe_dump(self.config.__dict__, f)

    def get_callback_list(self, model=None, eval_dataset=None, training=True):
        """
        Get callbacks which contains checkpoint callback, eval callback and loss callback.

        Args:
            model (Cell): The network is added callback (default=None).
            eval_dataset (Dataset): Dataset for eval (default=None).
        """

        callback_list = []
        if self.config.time_callback:
            time_cb = TimeMonitor()
            callback_list.append(time_cb)
        if not training:
            eval_callback = EvalCallBack(model, eval_dataset,
                                         eval_file_path=os.path.join(self.output_dir, 'Eval_Log'),
                                         ckpt_dir=self.output_dir)
            callback_list.append(eval_callback)
            return callback_list
        if self.config.save_checkpoint:
            config_ck = CheckpointConfig(save_checkpoint_steps=self.config.save_checkpoint_steps * self.step_per_epoch,
                                         keep_checkpoint_max=self.config.keep_checkpoint_max)
            ckpt_cb = ModelCheckpoint(prefix=self.config.algorithm,
                                      directory=self.output_dir,
                                      config=config_ck)
            callback_list.append(ckpt_cb)
        if self.config.eval_callback:
            if model is None:
                raise RuntimeError("config.eval_callback is {}; get_callback_list() args model is {}".format(
                    self.config.eval_callback, model))
            if eval_dataset is None:
                raise RuntimeError("config.eval_callback is {}; get_callback_list() "
                                   "args eval_dataset is {}".format(self.config.eval_callback, eval_dataset))
            eval_callback = EvalCallBack(model, eval_dataset,
                                         eval_file_path=os.path.join(self.output_dir, 'Eval_Log'),
                                         ckpt_dir=self.output_dir)
            callback_list.append(eval_callback)
        if self.config.loss_callback:
            loss_callback = LossCallBack(loss_file_path=os.path.join(self.output_dir,
                                                                     'Loss_Log'))
            callback_list.append(loss_callback)
        if callback_list:
            return callback_list
        return None

    def get_train_eval_net(self):
        if self.config.algorithm == 'MDLF':
            net = MDLF(self.config.feature_num,
                       self.config.embedding_size_step,
                       self.config.embedding_size,
                       self.config.state_size,
                       self.config.intervals,
                       self.config.length,
                       self.config.MAX_SPARSE)
        elif self.config.algorithm == 'BasicDLF':
            net = BasicDLF(self.config.feature_num,
                           self.config.embedding_size_step,
                           self.config.embedding_size,
                           self.config.state_size,
                           self.config.intervals,
                           self.config.length,
                           self.config.MAX_SPARSE)
        elif self.config.algorithm == 'Hierarch':
            net = Hierarch(self.config.feature_num,
                           self.config.embedding_size,
                           self.config.intervals,
                           self.config.length,
                           self.config.MAX_SPARSE)
        elif self.config.algorithm == 'DeepHit':
            net = DeepHit(self.config.feature_num,
                          self.config.embedding_size,
                          self.config.intervals,
                          self.config.length,
                          self.config.MAX_SPARSE)
        elif self.config.algorithm == 'DWPP':
            net = DWPP(self.config.feature_num,
                       self.config.embedding_size,
                       self.config.intervals,
                       self.config.length,
                       self.config.interval_length,
                       self.config.MAX_SPARSE)
        elif self.config.algorithm == 'ADM':
            net = ADM(self.config.feature_num,
                      self.config.embedding_size,
                      self.config.intervals,
                      self.config.length,
                      self.config.MAX_SPARSE)
        else:
            raise NotImplementedError
        if self.config.algorithm == 'MDLF' or self.config.algorithm == 'BasicDLF' \
                or self.config.algorithm == 'DWPP' or self.config.algorithm == 'ADM':
            loss_net = LFLossClass(net, self.config.algorithm,
                                   [self.config.length, self.config.intervals, self.config.interval_length],
                                   [self.config.w_win_f, self.config.w_win_w, self.config.w_win_F,
                                    self.config.w_lose_F, self.config.neighbor])
        else:
            loss_net = SALossClass(net, self.config.algorithm,
                                   [self.config.length, self.config.intervals, self.config.interval_length],
                                   [self.config.w_l1, self.config.w_l2, self.config.w_l3, self.config.w_l4])
        train_net = TrainStepWrap(loss_net, step_per_epoch=self.step_per_epoch, lr=self.config.lr,
                                  eps=self.config.eps, beta1=self.config.beta1, beta2=self.config.beta2,
                                  amsgrad=self.config.amsgrad)
        eval_net = EvalNet(net)
        return train_net, eval_net
