# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Train Foveabox and get checkpoint files."""
from pathlib import Path
from pprint import pprint
import logging
from datetime import datetime
from functools import reduce
import mindspore as ms

from mindspore.communication.management import init, get_rank, get_group_size
from mindspore.train import Model
from mindspore.context import ParallelMode

from mindspore.common import set_seed

from src.foveabox.foveabox import Foveabox, FoveaboxInfer
from src.net_with_loss import (
    WithLossCell, TrainOneStepCell, LossNet, TrainOneStepCellCPU
)
from src.dataset import prepare_data
from src.config import get_config, Config
from src.common import (
    config_logging, get_callbacks, get_device_id, get_optimizer,
    dump_env_and_params
)
from src.mlflow_funcs import (
    mlflow_log_state, mlflow_log_args,
)


def load_ckpt_to_network(net, checkpoint_path, finetune=False):
    """Load pre-trained checkpoint."""
    new_param = {}
    if finetune:
        param_not_load = [
            'learning_rate',
            'stat.bbox_head.conv_cls.weight', 'stat.bbox_head.conv_cls.bias',
            'bbox_head.conv_cls.weight', 'bbox_head.conv_cls.bias',
            'accum.bbox_head.conv_cls.weight', 'accum.bbox_head.conv_cls.bias',
        ]
    else:
        param_not_load = []

    logging.info('Loading from checkpoint: %s', checkpoint_path)
    param_dict = ms.load_checkpoint(checkpoint_path)

    for key, value in param_dict.items():
        if key.startswith('bbox_head.'):
            key = key.replace('gn.weight', 'norm.gamma')\
               .replace('gn.bias', 'norm.beta')\
               .replace('fovea_cls', 'conv_cls')\
               .replace('fovea_reg', 'conv_reg')
        if key in param_not_load:
            continue
        if not key.startswith('net.'):
            new_param['net.' + key] = value
        else:
            new_param[key] = value

    ms.load_param_into_net(net, new_param)

    logging.info('\tDone!\n')
    return net


def flatten_dict(cfg_dict, parent_key='', sep='.'):
    """process values before parameters saving"""
    res_list = []
    for k, v in cfg_dict.items():
        new_key = f'{parent_key}{sep}{k}' if parent_key else k
        if isinstance(v, Config):
            res_list.extend(
                flatten_dict(v.__dict__, parent_key=new_key, sep=sep).items()
            )
        else:
            res_list.append(
                (
                    new_key,
                    str(v) if len(str(v)) < 500 else str(v)[:477] + '...'
                )
            )

    return dict(res_list)


def main():
    """Training entry point."""
    config = get_config()
    mlflow_log_state()
    config_dict = config.__dict__
    config_dict = flatten_dict(config_dict)
    mlflow_log_args(config_dict)

    set_seed(1)
    mode = ms.PYNATIVE_MODE if config.pynative_mode else ms.GRAPH_MODE

    ms.set_context(mode=mode, device_target=config.device_target,
                   device_id=get_device_id(), max_call_depth=2000)

    if config.device_target == 'GPU':
        ms.set_context(enable_graph_kernel=bool(config.enable_graph_kernel))
    if config.run_distribute:
        init()
        rank = get_rank()
        device_num = get_group_size()
        ms.set_auto_parallel_context(
            device_num=device_num, parallel_mode=ParallelMode.DATA_PARALLEL,
            gradients_mean=True)
    else:
        rank = 0
        device_num = 1
    config.rank_id = rank
    config.device_num = device_num

    pprint(config)
    logging.info(
        'Please check the above information for the configurations\n\n')
    experiment_name = datetime.now().strftime('%y%m%d_%H%M%S') + '_Foveabox'
    if config.brief is not None:
        experiment_name = f'{experiment_name}_{config.brief}'
    ckpt_save_dir = Path(config.train_outputs) / experiment_name
    if config.rank_id == 0:
        ckpt_save_dir.mkdir(parents=True, exist_ok=True)
        dump_env_and_params(ckpt_save_dir, config)
    config_logging(filename_prefix=str(ckpt_save_dir / 'logs' / 'train'))

    train_dataset, val_dataset = prepare_data(config)
    train_dataset_size = train_dataset.get_dataset_size()
    val_dataset_size = (
        None if val_dataset is None else val_dataset.get_dataset_size()
    )
    logging.info('\nTrain size: %s\nValid size: %s',
                 train_dataset_size, val_dataset_size)
    logging.info("Creating network...")
    net = Foveabox(config=config)

    logging.info(
        'Number of parameters: %s',
        str(sum(reduce(lambda x, y: x * y, p.shape)
                for p in net.trainable_params() + net.untrainable_params()))
    )
    # load pretrained checkpoint
    if config.pre_trained:
        net = load_ckpt_to_network(net, config.pre_trained, config.finetune)

    criterion = LossNet()
    net_with_loss = WithLossCell(net, criterion)

    logging.info('Device type: %s', config.device_target)
    logging.info('Creating criterion, lr and opt objects...')

    opt = get_optimizer(config, net, train_dataset_size)

    logging.info("\tDone!\n")
    if config.device_target == "CPU":
        net_with_loss = TrainOneStepCellCPU(
            net_with_loss, opt, sens=config.loss_scale)
    else:
        net_with_loss = TrainOneStepCell(
            net_with_loss, opt, scale_sense=config.loss_scale,
            grad_clip=config.grad_clip)

    model = Model(net_with_loss)

    cb = get_callbacks(
        arch='Foveabox', logs_dir=ckpt_save_dir / 'logs',
        rank=config.rank_id, ckpt_dir=ckpt_save_dir,
        train_data_size=train_dataset_size, val_data_size=val_dataset_size,
        best_ckpt_dir=ckpt_save_dir / 'best_ckpt',
        summary_dir=ckpt_save_dir / 'summary',
        ckpt_save_every_step=config.save_every,
        print_loss_every=config.print_loss_every,
        ckpt_keep_num=config.keep_checkpoint_max,
        best_ckpt_num=config.keep_best_checkpoints_max,
        val_dataset=val_dataset, infer_net=FoveaboxInfer(net),
        val_dataset_dir=config.val_dataset, anno_fname='labels.json')

    model.train(
        config.epoch_size, train_dataset,
        callbacks=cb,
        dataset_sink_mode=bool(config.datasink))


if __name__ == '__main__':
    main()
