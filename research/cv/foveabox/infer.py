# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Foveabox inference."""
import argparse
import os
import json

from pprint import pprint
from pathlib import Path

import mindspore as ms

import cv2
import numpy as np
from tqdm import tqdm

from src.foveabox.foveabox import Foveabox, FoveaboxInfer
from src.config import (
    parse_yaml, parse_cli_to_yaml, merge, Config, compute_features_info
)
from src.dataset import (
    imnormalize_column, rescale_column_test, resize_column_test
)


def get_config():
    """
    Get Config according to the yaml file and cli arguments.
    """
    parser = argparse.ArgumentParser(description='default name',
                                     add_help=False)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--config_path', type=str,
                        default=os.path.join(current_dir, 'configs',
                                             'resnet50_config.yaml'),
                        help='Config file path')
    parser.add_argument('--checkpoint_path',
                        help='Path to save checkpoint.')
    parser.add_argument('--pred_output',
                        help='Path to model predictions.')
    parser.add_argument('--pred_input',
                        help='Path to model predictions.')
    path_args, _ = parser.parse_known_args()
    default, helper, choices = parse_yaml(path_args.config_path)
    args = parse_cli_to_yaml(parser=parser, cfg=default, helper=helper,
                             choices=choices, cfg_path=path_args.config_path)
    final_config = Config(merge(args, default))
    final_config = compute_features_info(final_config)
    pprint(final_config)
    print('Please check the above information for the configurations',
          flush=True)
    return final_config


def imread(path):
    """Read image."""
    img_bgr = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
    return img_rgb


def data_loader(path: Path, config):
    """Load image or images from folder in generator."""
    def process_img(img):
        data = (
            img, img.shape[:2], np.zeros((0, 4)), np.zeros((0, 1)),
            np.zeros((0, 1))
        )
        if config.keep_ratio:
            data = rescale_column_test(*data, config=config)
        else:
            data = resize_column_test(*data, config=config)
        img, meta, *_ = imnormalize_column(*data, config=config)
        img = img.transpose(2, 0, 1).copy()
        img = img.astype(np.float32)
        return img, meta

    extensions = ('.png', '.jpg', '.jpeg')
    path = path.resolve()
    if path.is_dir():
        for item in path.iterdir():
            if item.is_dir():
                continue
            if item.suffix.lower() not in extensions:
                continue
            image = imread(str(item))
            image = image[..., ::-1].copy()
            tensor, img_meta = process_img(image)

            yield (
                str(item), ms.Tensor(img_meta[np.newaxis, :]),
                ms.Tensor(tensor[np.newaxis, :])
            )
    else:
        image = imread(str(path))
        image = image[..., ::-1].copy()
        tensor, img_meta = process_img(image)

        yield (
            str(path), ms.Tensor(img_meta[np.newaxis, :]),
            ms.Tensor(tensor[np.newaxis, :])
        )


def main():
    config = get_config()
    ms.context.set_context(
        mode=ms.context.PYNATIVE_MODE, device_target=config.device_target
    )

    net = Foveabox(config)
    net = FoveaboxInfer(net)

    param_dict = ms.load_checkpoint(config.checkpoint_path)
    param_dict_new = {}
    for key, value in param_dict.items():
        if key.startswith('bbox_head.'):
            key = key.replace('gn.weight', 'norm.gamma') \
                .replace('gn.bias', 'norm.beta') \
                .replace('fovea_cls', 'conv_cls') \
                .replace('fovea_reg', 'conv_reg')
        if not key.startswith('net.'):
            param_dict_new['net.' + key] = value
        else:
            param_dict_new[key] = value
    ms.load_param_into_net(net.net, param_dict_new)
    net.set_train(False)

    data_generator = data_loader(Path(config.pred_input), config)

    predictions = {}
    for name, meta, tensor in tqdm(list(data_generator)):
        outputs = net(tensor, meta)
        bbox_preds, cls_scores = outputs

        cls_scores = [i.asnumpy() for i in cls_scores]
        bbox_preds = [i.asnumpy() for i in bbox_preds]
        meta = [i.asnumpy() for i in meta]

        bboxes, labels = net.net.bbox_head.get_bboxes(
            cls_scores=cls_scores,
            bbox_preds=bbox_preds,
            img_metas=meta
        )

        bboxes_squee = np.squeeze(bboxes, axis=0)
        labels_mask = np.squeeze(labels, axis=0)

        meta = meta[0]
        bboxes_mask = bboxes_squee[:, :4]
        bboxes_mask[::, [2, 3]] -= bboxes_mask[::, [0, 1]] - 1

        confs_mask = bboxes_squee[:, 4]

        predictions[name] = {
            'height': int(meta[0]),
            'width': int(meta[1]),
            'predictions': []
        }

        for box, conf, label in zip(bboxes_mask, confs_mask, labels_mask):
            predictions[name]['predictions'].append({
                'bbox': {
                    'x_min': float(box[0]),
                    'y_min': float(box[1]),
                    'width': float(box[2]),
                    'height': float(box[3])
                },
                'class': {
                    'label': int(label),
                    'category_id': 'unknown',
                    'name': config.coco_classes[int(label) + 1]
                },
                'score': float(conf)
            })

    with open(config.pred_output, 'w', encoding='utf-8') as f:
        json.dump(predictions, f, indent=1)


if __name__ == '__main__':
    main()
