# Builtin Configurations(DO NOT CHANGE THESE CONFIGURATIONS unless you know exactly what you are doing)
enable_modelarts: False
data_url: ""
train_url: "/cache/data/grid_rcnn_models"
checkpoint_url: ""
data_path: "/cache/data"
output_path: "/cache/train"
load_path: "/cache/checkpoint_path"
device_target: GPU
pynative_mode: 0  # run in PYNATIVE mode (1) or in GRAPH mode (0)
enable_profiling: False

train_outputs: '/data/grid_rcnn_models'
# ==============================================================================
# data
img_width: 1024
img_height: 1024
divider: 64
img_mean: [123.675, 116.28, 103.53]
img_std: [58.395, 57.12, 57.375]
to_rgb: True
# The value of keep_ratio must be the same as the [KEEP_RATIO] parameter in Ascend310 inference
keep_ratio: True
flip_ratio: 0.5
expand_ratio: 0.0

# backbone
backbone:
  type: "resnet"
  depth: 50
backbone_pretrained: '/repo/huawei-mindspore/grid_r_cnn_test/resnet50.ckpt'
resnet_frozen_stages: 1
resnet_norm_eval: True
resnet_num_stages: 4
resnet_out_indices: [0, 1, 2, 3]
resnext_groups: 1
resnext_base_width: 4

# fpn
fpn_in_channels: [256, 512, 1024, 2048]
fpn_out_channels: 256
fpn_num_outs: 5

# rpn
rpn_in_channels: 256
rpn_feat_channels: 256
rpn_loss_cls_weight: 1.0
rpn_loss_reg_weight: 1.0
rpn_cls_out_channels: 1
rpn_target_means: [0., 0., 0., 0.]
rpn_target_stds: [1.0, 1.0, 1.0, 1.0]
smoothl1_beta: 0.1111111111111


# anchor
anchor_scales: [8]
anchor_ratios: [0.5, 1.0, 2.0]
anchor_strides: [4, 8, 16, 32, 64]
num_anchors: 3

# bbox_assign_sampler
neg_iou_thr: 0.3
pos_iou_thr: 0.7
min_pos_iou: 0.3
num_gts: 128
num_expected_neg: 256
num_expected_pos: 128

# proposal
activate_num_classes: 2
use_sigmoid_cls: True

# bbox_assign_sampler_stage2
neg_iou_thr_stage2: 0.5
pos_iou_thr_stage2: 0.5
min_pos_iou_stage2: 0.5
num_bboxes_stage2: 2000
num_expected_pos_stage2: 128
num_expected_neg_stage2: 512

# roi_align
roi_layer: {type: 'RoIAlign', out_size: 7, sample_num: 2}
roi_align_out_channels: 256
roi_align_featmap_strides: [4, 8, 16, 32]
roi_align_finest_scale: 56
roi_sample_num: 640

# rcnn
#reg_class_agnostic=False
rcnn_num_fcs: 2
rcnn_in_channels: 256
rcnn_fc_out_channels: 1024
rcnn_loss_cls_weight: 1

# grid roi
grid_roi_layer: {type: 'RoIAlign', out_size: 14, sample_num: 2}
grid_roi_align_out_channels: 256
grid_roi_align_featmap_strides: [4, 8, 16, 32]
grid_roi_align_finest_scale: 56
grid_roi_sample_num: 128

# grid
grid_max_num: 192
grid_num_convs: 8
grid_in_channels: 256
grid_conv_out_channels: 576
num_grids: 9
grid_mask_size: 56
grid_radius: 1
grid_loss_weight: 15

# train proposal
rpn_proposal_nms_across_levels: False
rpn_proposal_nms_pre: 2000
rpn_proposal_nms_post: 2000
rpn_proposal_max_num: 2000
rpn_proposal_nms_thr: 0.7
rpn_proposal_min_bbox_size: 0

# test proposal
rpn_nms_across_levels: False
rpn_nms_pre: 1000
rpn_nms_post: 1000
rpn_max_num: 1000
rpn_nms_thr: 0.7
rpn_min_bbox_min_size: 0
test_score_thr: 0.03
test_iou_thr: 0.3
test_max_per_img: 100
test_batch_size: 1

# LR
base_lr: 0.01
warmup_step: 3665
warmup_ratio: 0.0125
milestones: [17, 23]
sgd_momentum: 0.9
lr_type: "dynamic"

# train
batch_size: 4
loss_scale: 256
weight_decay: 0.0001
epoch_size: 25
run_eval: True
eval_every: 3
enable_graph_kernel: False
interval: 1
opt_type: "sgd"
finetune: False
datasink: False

# Number of threads used to process the dataset in parallel
num_parallel_workers: 6
# Parallelize Python operations with multiple worker processes
python_multiprocessing: False

train_data_type: 'coco'
val_data_type: 'mindrecord'
train_dataset: '/data/coco-2017/train'
val_dataset: '/data/mindrecord_eval'
coco_classes: ['background', 'person', 'bicycle', 'car', 'motorcycle',
               'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
num_classes: 81
brief: 'gpu-1_1024x1024'

train_dataset_num: 117265
train_dataset_divider: 24

# train.py GridRcnn training
run_distribute: False
dataset: "coco"
pre_trained: ""
device_id: 0
device_num: 1
rank_id: 0
image_dir: ''
anno_path: ''
log_summary: False
grad_clip: 1.0

# eval.py GridRcnn evaluation
checkpoint_path: ""

# infer.py script
pred_input: ""
pred_output: ""

# callbacks
save_every: 100
keep_checkpoint_max: 5
keep_best_checkpoints_max: 5

---
# Config description for each option
enable_modelarts: 'Whether training on modelarts, default: False'
data_url: 'Dataset url for obs'
train_url: 'Training output url for obs'
data_path: 'Dataset path for local'
output_path: 'Training output path for local'
result_dir: "result files path."
label_dir: "image file path."

device_target: "device where the code will be implemented, default is Ascend"
file_name: "output file name."
dataset: "Dataset, either cifar10 or imagenet2012"
parameter_server: 'Run parameter server train'
width: 'input width'
height: 'input height'
enable_profiling: 'Whether enable profiling while training, default: False'
only_create_dataset: 'If set it true, only create Mindrecord, default is false.'
run_distribute: 'Run distribute, default is false.'
do_train: 'Do train or not, default is true.'
do_eval: 'Do eval or not, default is false.'
pre_trained: 'Pretrained checkpoint path'
device_id: 'Device id, default is 0.'
device_num: 'Use device nums, default is 1.'
rank_id: 'Rank id, default is 0.'
file_format: 'file format'
checkpoint_path: "Checkpoint file path."
ckpt_file: 'Checkpoint file path for model exporting'
result_path: "result file path."
backbone: "backbone network name, options:resnet, resnext"
interval: "val interval"

---
dataset: ['coco', 'mindrecord']
device_target: ['GPU', 'CPU']
export_file_format: ["AIR", "ONNX", "MINDIR"]
