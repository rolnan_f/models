# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
"""
Calculate bounding box overlaps (common area).

FP16 Contributed by https://github.com/open-mmlab/mmdetection/pull/4889
Note:
    Assume bboxes1 is M x 4, bboxes2 is N x 4, when mode is 'iou',
    there are some new generated variable when calculating IOU
    using bbox_overlaps function:

    1) is_aligned is False
        area1: M x 1
        area2: N x 1
        lt: M x N x 2
        rb: M x N x 2
        wh: M x N x 2
        overlap: M x N x 1
        union: M x N x 1
        ious: M x N x 1

        Total memory:
            S = (9 x N x M + N + M) * 4 Byte,

        When using FP16, we can reduce:
            R = (9 x N x M + N + M) * 4 / 2 Byte
            R large than (N + M) * 4 * 2 is always true when N and M >= 1.
            Obviously, N + M <= N * M < 3 * N * M, when N >=2 and M >=2,
                       N + 1 < 3 * N, when N or M is 1.

        Given M = 40 (ground truth), N = 400000 (three anchor boxes
        in per grid, FPN, R-CNNs),
            R = 275 MB (one times)

        A special case (dense detection), M = 512 (ground truth),
            R = 3516 MB = 3.43 GB

        When the batch size is B, reduce:
            B x R

        Therefore, CUDA memory runs out frequently.

        Experiments on GeForce RTX 2080Ti (11019 MiB):

        |   dtype   |   M   |   N    |   Use    |   Real   |   Ideal   |
        |:---------:|:-----:|:------:|:--------:|:--------:|:---------:|
        |   FP32    |  512  | 400000 | 8020 MiB |    --    |    --     |
        |   FP16    |  512  | 400000 | 4504 MiB | 3516 MiB | 3516 MiB  |
        |   FP32    |  40   | 400000 | 1540 MiB |    --    |    --     |
        |   FP16    |  40   | 400000 | 1264 MiB | 276 MiB  | 275 MiB   |

    2) is_aligned is True
        area1: N x 1
        area2: N x 1
        lt: N x 2
        rb: N x 2
        wh: N x 2
        overlap: N x 1
        union: N x 1
        ious: N x 1

        Total memory:
            S = 11 x N * 4 Byte

        When using FP16, we can reduce:
            R = 11 x N * 4 / 2 Byte

    So do the 'giou' (large than 'iou').

    Time-wise, FP16 is generally faster than FP32.

    When gpu_assign_thr is not -1, it takes more time on cpu
    but not reduce memory.
    There, we can reduce half the memory and keep the speed.

If ``is_aligned`` is ``False``, then calculate the overlaps between each
bbox of bboxes1 and bboxes2, otherwise the overlaps between each aligned
pair of bboxes1 and bboxes2.
"""

import numpy as np
import mindspore as ms
from mindspore import ops


def bbox_overlaps(bboxes1, bboxes2, mode='iou'):
    """Calculate the ious between each bbox of bboxes1 and bboxes2.

    Args:
        bboxes1(ndarray): shape (n, 4)
        bboxes2(ndarray): shape (k, 4)
        mode(str): iou (intersection over union) or iof (intersection
            over foreground)

    Returns:
        ious(ndarray): shape (n, k)
    """

    assert mode in ['iou', 'iof']

    bboxes1 = bboxes1.astype(np.float32)
    bboxes2 = bboxes2.astype(np.float32)
    rows = bboxes1.shape[0]
    cols = bboxes2.shape[0]
    ious = np.zeros((rows, cols), dtype=np.float32)
    if rows * cols == 0:
        return ious
    exchange = False
    if bboxes1.shape[0] > bboxes2.shape[0]:
        bboxes1, bboxes2 = bboxes2, bboxes1
        ious = np.zeros((cols, rows), dtype=np.float32)
        exchange = True
    area1 = (bboxes1[:, 2] - bboxes1[:, 0] + 1) * (bboxes1[:, 3] - bboxes1[:, 1] + 1)
    area2 = (bboxes2[:, 2] - bboxes2[:, 0] + 1) * (bboxes2[:, 3] - bboxes2[:, 1] + 1)
    for i in range(bboxes1.shape[0]):
        x_start = np.maximum(bboxes1[i, 0], bboxes2[:, 0])
        y_start = np.maximum(bboxes1[i, 1], bboxes2[:, 1])
        x_end = np.minimum(bboxes1[i, 2], bboxes2[:, 2])
        y_end = np.minimum(bboxes1[i, 3], bboxes2[:, 3])
        overlap = np.maximum(x_end - x_start + 1, 0) * np.maximum(
            y_end - y_start + 1, 0)
        if mode == 'iou':
            union = area1[i] + area2 - overlap
        else:
            union = area1[i] if not exchange else area2
        ious[i, :] = overlap / union
    if exchange:
        ious = ious.T
    return ious


def bbox_overlaps_ms(bboxes1, bboxes2, mode='iou', is_aligned=False, eps=1e-6):
    """Calculate overlap between two set of bboxes.

    Args:
        bboxes1 (Tensor): shape (B, m, 4) in <x1, y1, x2, y2> format or empty.
        bboxes2 (Tensor): shape (B, n, 4) in <x1, y1, x2, y2> format or empty.
            B indicates the batch dim, in shape (B1, B2, ..., Bn).
            If ``is_aligned`` is ``True``, then m and n must be equal.
        mode (str): "iou" (intersection over union), "iof" (intersection over
            foreground) or "giou" (generalized intersection over union).
            Default "iou".
        is_aligned (bool, optional): If True, then m and n must be equal.
            Default False.
        eps (float, optional): A value added to the denominator for numerical
            stability. Default 1e-6.

    Returns:
        Tensor: shape (m, n) if ``is_aligned`` is False else shape (m,)
    """
    bboxes1 = ops.cast(bboxes1, ms.float16)
    bboxes2 = ops.cast(bboxes2, ms.float16)

    assert mode in ['iou', 'iof', 'giou'], f'Unsupported mode {mode}'
    # Either the boxes are empty or the length of boxes' last dimension is 4
    assert (bboxes1.shape[-1] == 4 or bboxes1.shape[0] == 0)
    assert (bboxes2.shape[-1] == 4 or bboxes2.shape[0] == 0)

    # Batch dim must be the same
    # Batch dim: (B1, B2, ... Bn)
    assert bboxes1.shape[:-2] == bboxes2.shape[:-2]
    batch_shape = bboxes1.shape[:-2]

    rows = bboxes1.shape[-2]
    cols = bboxes2.shape[-2]
    if is_aligned:
        assert rows == cols

    if rows * cols == 0:
        if is_aligned:
            return bboxes1.new(batch_shape + (rows,))
        return bboxes1.new(batch_shape + (rows, cols))

    area1 = (bboxes1[..., 2] - bboxes1[..., 0]) * (
        bboxes1[..., 3] - bboxes1[..., 1])
    area2 = (bboxes2[..., 2] - bboxes2[..., 0]) * (
        bboxes2[..., 3] - bboxes2[..., 1])

    if is_aligned:
        lt = ops.max(bboxes1[..., :2], bboxes2[..., :2])  # [B, rows, 2]
        rb = ops.min(bboxes1[..., 2:], bboxes2[..., 2:])  # [B, rows, 2]

        wh = rb - lt
        overlap = wh[..., 0] * wh[..., 1]

        if mode in ['iou', 'giou']:
            union = area1 + area2 - overlap
        else:
            union = area1
        enclosed_lt = ops.min(bboxes1[..., :2], bboxes2[..., :2])
        enclosed_rb = ops.max(bboxes1[..., 2:], bboxes2[..., 2:])
    else:
        print(bboxes1.shape, bboxes2.shape)
        rows, cols = bboxes1.shape[0], bboxes2.shape[0]
        bboxes1_br = ops.broadcast_to(ops.expand_dims(bboxes1, 1),
                                      (rows, cols, 4))
        bboxes2_br = ops.broadcast_to(ops.expand_dims(bboxes2, 0),
                                      (rows, cols, 4))
        lt = ops.maximum(bboxes1_br[..., :2],
                         bboxes2_br[..., :2])  # [B, rows, cols, 2]
        rb = ops.minimum(bboxes1_br[..., 2:],
                         bboxes2_br[..., 2:])  # [B, rows, cols, 2]

        wh = rb - lt
        overlap = wh[..., 0] * wh[..., 1]  # rows (bb1) x cols (bb2)

        if mode in ['iou', 'giou']:
            union = area1[..., None] + area2[..., None, :] - overlap
        else:
            union = area1[..., None]
        enclosed_lt = ops.minimum(bboxes1[..., :, None, :2],
                                  bboxes2[..., None, :, :2])
        enclosed_rb = ops.maximum(bboxes1[..., :, None, 2:],
                                  bboxes2[..., None, :, 2:])

    eps = ms.Tensor([eps], dtype=ms.float32)
    union = ops.maximum(union, eps)
    ious = overlap / union
    if mode == 'giou':
        # calculate gious
        enclose_wh = enclosed_rb - enclosed_lt
        enclose_area = enclose_wh[..., 0] * enclose_wh[..., 1]
        enclose_area = ops.maximum(enclose_area, eps)
        ious = ious - (enclose_area - union) / enclose_area
    return ious
