# Contents

* [Guided Anchoring Description](#guided-anchoring-description)
    * [Model Architecture](#model-architecture)
    * [Dataset](#dataset)
* [Environment Requirements](#environment-requirements)
* [Quick Start](#quick-start)
    * [Prepare the model](#prepare-the-model)
    * [Run the scripts](#run-the-scripts)
* [Script Description](#script-description)
    * [Script and Sample Code](#script-and-sample-code)
    * [Script Parameters](#script-parameters)
* [Training](#training)
    * [Training process](#training-process)
* [Evaluation](#evaluation)
    * [Evaluation process](#evaluation-process)
        * [Evaluation on GPU](#evaluation-on-gpu)
    * [Evaluation result](#evaluation-result)
* [Model Description](#model-description)
    * [Performance](#performance)
* [Description of Random Situation](#description-of-random-situation)
* [ModelZoo Homepage](#modelzoo-homepage)

## [Guided Anchoring Description](#contents)

Region anchors are the cornerstone of modern object detection techniques.
State-of-the-art detectors mostly rely on a dense anchoring scheme,
where anchors are sampled uniformly over the spatial domain with a predefined
set of scales and aspect ratios. In this paper, authors revisit this
foundational stage. Our study shows that it can be done much more effectively
and efficiently. Specifically, they present an alternative scheme, named
Guided Anchoring, which leverages semantic features to guide the anchoring.
The proposed method jointly predicts the locations where the center of objects
of interest are likely to exist as well as the scales and aspect ratios at
different locations. On top of predicted anchor shapes, authors mitigate the
feature inconsistency with a feature adaption module. They also study the use
of high-quality proposals to improve detection performance. The anchoring scheme
can be seamlessly integrated into proposal methods and detectors.

[Paper](https://arxiv.org/pdf/1901.03278.pdf): Jiaqi Wang, Kai Chen, Shuo Yang,
Chen Change Loy, Dahua Lin. Region Proposal by Guided Anchoring (2019).

### [Model Architecture](#contents)

The model has two configurations based on the RetinaNet and Faster RCNN architectures.
Instead of using predefined anchors, authors suggest an additional mechanism called Guided Anchoring
which is used during inference to predict more appropriate anchors.
Guided anchoring consists of two pars: Anchor generator and Feature adaption.

### [Dataset](#contents)

Note that you can run the scripts based on the dataset mentioned in original
paper or widely used in relevant domain/network architecture. In the following
sections, we will introduce how to run the scripts using the related dataset
below.

Dataset used: [COCO-2017](https://cocodataset.org/#download)

* Dataset size: 25.4G
    * Train: 18.0G，118287 images
    * Val: 777.1M，5000 images
    * Test: 6.2G，40670 images
    * Annotations: 474.7M, represented in 3 JSON files for each subset.
* Data format: image and json files.
    * Note: Data will be processed in dataset.py

## [Environment Requirements](#contents)

* Install [MindSpore](https://www.mindspore.cn/install/en).
* Download the dataset COCO-2017.
* Install third-parties requirements:

```text
numpy~=1.21.6
onnxruntime~=1.13.1
opencv-python~=4.8.1
pycocotools~=2.0.7
PyYAML~=6.0
seaborn~=0.12.2
tqdm~=4.64.1
```

* We use COCO-2017 as training dataset in this example by default, and you
 can also use your own datasets. Dataset structure:

```shell
.
└── coco-2017
    ├── train
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    ├── validation
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    └── test
        ├── data
        │    ├── 000000000001.jpg
        │    ├── 000000000002.jpg
        │    └── ...
        └── labels.json
```

* Also we make evaluation only on mindrecord converted dataset. Use the
 `convert_dataset.py` script to convert original COCO subset to mindrecord.

```shell
python convert_dataset.py --config_path default_config.yaml --converted_coco_path data/coco-2017/validation --converted_mindrecord_path data/mindrecord/validation
```

Result mindrecord dataset will have next format:

```shell
.
└── mindrecord
    ├── validation
    │   ├── file.mindrecord
    │   ├── file.mindrecord.db
    │   └── labels.json
    └── test
        ├── file.mindrecord
        ├── file.mindrecord.db
        └── labels.json
```

It is possible to convert train dataset to mindrecord format and use it to
train model.

## [Quick Start](#contents)

### [Prepare the model](#contents)

1. Prepare yaml config file. Create file and copy content from
 `default_config.yaml` to created file.
2. Change data settings: experiment folder (`train_outputs`), image size
 settings (`img_width`, `img_height`, etc.), subsets folders (`train_dataset`,
 `val_dataset`, `train_data_type`, `val_data_type`), information about
 categories etc.
3. Change the backbone settings: type (`backbone.type`), path to pretrained
 ImageNet weights (`backbone.pretrained`), layer freezing settings
 (`backbone.frozen_stages`).
4. Change other training hyper parameters (learning rate, regularization,
 augmentations etc.).
5. Prepare pre-trained checkpoints.

### [Run the scripts](#contents)

After installing MindSpore via the official website, you can start training and
evaluation as follows:

* running on GPU

```shell
# standalone training on GPU
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH]

# run eval on GPU
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [CHECKPOINT_PATH]
```

## [Script Description](#contents)

### [Script and Sample Code](#contents)

```commandline
guided_anchoring
├── eval.py  # Eval script
├── export.py  # Export script
├── requirements.txt  # List of requiremetns
├── scripts  # Main scripts
│   ├── run_eval_gpu.sh  # Script to run evaluation
│   ├── run_standalone_train_gpu.sh  # Script to run training
│   └── build_custom_layers.sh  # Build DeformConv2d, MaskedConv2d layers
├── src
│   ├── __init__.py
│   ├── callbacks.py  # Callbacks
│   ├── common.py  # Common utils
│   ├── config.py  # Script to parse config yaml and merge with CLI
│   ├── dataset.py  # Data loading and preprocessing
│   ├── detecteval.py  # Eval utils
│   ├── eval_utils.py  # Eval utils
│   ├── guided_anchoring
│   │   ├── __init__.py
│   │   ├── anchor
│   │   │   ├── __init__.py
│   │   │   ├── anchor_generator.py  # Anchor generator
│   │   │   ├── creators.py  # Instance builders
│   │   │   └── utils.py  # Aux utils for generating anchors
│   │   ├── backbones
│   │   │   ├── __init__.py
│   │   │   ├── creators.py  # Instance builders
│   │   │   ├── resnet.py  # Resnet model implementation
│   │   │   └── resnext.py  # # Resnext model implementation
│   │   ├── bbox
│   │   │   ├── __init__.py
│   │   │   ├── assigners
│   │   │   │   ├── __init__.py
│   │   │   │   ├── approx_max_iou_assigner.py  # Approximate max IoU assigner
│   │   │   │   ├── assign_result.py  # Class to collect results of assignment
│   │   │   │   └── max_iou_assigner.py  # Usual max IoU assigner
│   │   │   ├── bbox_nms.py  # Non-maximum suppression utils
│   │   │   ├── creators.py  # Instance builders
│   │   │   ├── delta_xywh_bbox_coder.py  # Bbox coder
│   │   │   ├── iou_calculator.py  # IoU calculator
│   │   │   ├── multiclass_nms.py  # Non-maximum suppression for multiple classes
│   │   │   └── samplers
│   │   │       ├── __init__.py
│   │   │       ├── pseudo_sampler.py  # Pseudo sampler
│   │   │       ├── random_sampler.py  # Random sampler
│   │   │       └── sampling_result.py  # Class to collect results of sampling
│   │   ├── cuda
│   │   │   ├── custom_aot_extra.h
│   │   │   ├── masked_conv2d_cuda_kernel.cu
│   │   │   └── roi_align_cuda_kernel.cu
│   │   ├── detectors
│   │   │   ├── __init__.py
│   │   │   ├── creators.py  # Instance builders
│   │   │   ├── faster_rcnn.py  # Faster RCNN model implementation
│   │   │   └── retina_net.py  # RetinaNet model implementation
│   │   ├── heads
│   │   │   ├── __init__.py
│   │   │   ├── creators.py  # Instance builders
│   │   │   ├── ga_retina_head.py  # Head for RetinaNet with guided-anchoring mechanism
│   │   │   ├── ga_rpn_head.py  # Head for Faster RCNN with guided-anchoring mechanism
│   │   │   └── guided_anchor_head.py  # Common GA head class
│   │   ├── losses
│   │   │   ├── __init__.py
│   │   │   ├── creators.py  # Instance builders
│   │   │   ├── focal_loss.py  # Focal loss
│   │   │   └── iou_loss.py  # IoU loss
│   │   ├── misc.py  # Models utils
│   │   ├── necks
│   │   │   ├── __init__.py
│   │   │   ├── creators.py  # Instance builders
│   │   │   └── fpn.py  # FPN neck implementation
│   │   ├── ops
│   │   │   ├── __init__.py
│   │   │   ├── deform_conv2d.py  # DeformConv2d layer
│   │   │   ├── masked_conv2d.py  # DeformConv2d layer
│   │   │   └── roi_align.py  # RoiAlign layer
│   │   └── roi_heads
│   │       ├── __init__.py
│   │       ├── bbox_heads
│   │       │   ├── __init__.py
│   │       │   ├── bbox_head.py  # Bbox head
│   │       │   └── convfc_bbox_head.py  # ConvFC head
│   │       ├── creators.py  # Instance builders
│   │       ├── roi_extractors
│   │       │   ├── __init__.py
│   │       │   └── single_level_roi_extractor.py  # RoI extractor
│   │       ├── standard_roi_head.py  # RoI head
│   │       └── transforms.py  # RoI and bbox converters
│   ├── mlflow_functions.py  # Aux tools to log in MLFlow
│   ├── ms_configs  # Guided anchoring configs
│   │   ├── ga_faster_r101_caffe_fpn_1x_coco.yaml
│   │   ├── ga_faster_r50_caffe_fpn_1x_coco.yaml
│   │   ├── ga_faster_x101_32x4d_fpn_1x_coco.yaml
│   │   ├── ga_faster_x101_64x4d_fpn_1x_coco.yaml
│   │   ├── ga_retinanet_r101_caffe_fpn_1x_coco.yaml
│   │   ├── ga_retinanet_r50_caffe_fpn_1x_coco.yaml
│   │   ├── ga_retinanet_x101_32x4d_fpn_1x_coco.yaml
│   │   └── ga_retinanet_x101_64x4d_fpn_1x_coco.yaml
│   ├── net_with_loss.py  # Wrapper for network with loss
│   └── sheduler.py  # LR scheduler
└── train.py  # Main train script
```

### [Script Parameters](#contents)

Major parameters in the yaml config file as follows:

```yaml
# Builtin Configurations(DO NOT CHANGE THESE CONFIGURATIONS unless you know exactly what you are doing)
enable_modelarts: False
data_url: ""
train_url: "/cache/data/guided_anchoring_models"
checkpoint_url: ""
data_path: "/cache/data"
output_path: "/cache/train"
load_path: "/cache/checkpoint_path"
enable_profiling: False

train_outputs: '/data/guided_anchoring_models'
brief: 'gpu-1_1024x1024'
device_target: GPU
# ==============================================================================

# optimizer
opt_type: "sgd"
lr: 0.02
momentum: 0.9
weight_decay: 0.0001
warmup_step: 500
warmup_ratio: 0.001
lr_steps: [8, 11]
lr_type: "multistep"
grad_clip: 0


# train
num_gts: 100
batch_size: 4
test_batch_size: 1
loss_scale: 256
epoch_size: 12
run_eval: True
eval_every: 1
enable_graph_kernel: False
finetune: False
datasink: False
pre_trained: ''
pynative_mode: True

#distribution training
run_distribute: False
device_id: 0
device_num: 1
rank_id: 0

# Number of threads used to process the dataset in parallel
num_parallel_workers: 6
# Parallelize Python operations with multiple worker processes
python_multiprocessing: False

# dataset setting
train_data_type: 'coco'
val_data_type: 'mindrecord'
train_dataset: '/data/coco-2017/train'
val_dataset: '/data/mindrecord_eval'
coco_classes: ['person', 'bicycle', 'car', 'motorcycle',
               'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
num_classes: 81
train_dataset_num: 0
train_dataset_divider: 0

# images
img_width: 1024
img_height: 1024
divider: 32
img_mean: [103.53, 116.28, 123.675]
img_std: [1.0, 1.0, 1.0]
to_rgb: False
keep_ratio: False

# augmentation
flip_ratio: 0.5
expand_ratio: 0.0

# callbacks
save_every: 100
keep_checkpoint_max: 5
keep_best_checkpoints_max: 5

# ==============================================================================
model:
  backbone:
    pretrained: None
    depth: 101
    frozen_stages: 1
#    init_cfg:
#      checkpoint: open-mmlab://detectron2/resnet101_caffe
#      type: Pretrained
    norm_cfg:
      requires_grad: false
      type: BN
    norm_eval: true
    num_stages: 4
    out_indices: !!python/tuple
    - 0
    - 1
    - 2
    - 3
    style: caffe
    type: ResNet
  bbox_head:
    anchor_coder:
      target_means:
      - 0.0
      - 0.0
      - 0.0
      - 0.0
      target_stds:
      - 1.0
      - 1.0
      - 1.0
      - 1.0
      type: DeltaXYWHBBoxCoder
    approx_anchor_generator:
      octave_base_scale: 4
      ratios:
      - 0.5
      - 1.0
      - 2.0
      scales_per_octave: 3
      strides:
      - 8
      - 16
      - 32
      - 64
      - 128
      type: AnchorGenerator
    bbox_coder:
      target_means:
      - 0.0
      - 0.0
      - 0.0
      - 0.0
      target_stds:
      - 1.0
      - 1.0
      - 1.0
      - 1.0
      type: DeltaXYWHBBoxCoder
    feat_channels: 256
    in_channels: 256
    loc_filter_thr: 0.01
    loss_bbox:
      beta: 0.04
      loss_weight: 1.0
      type: SmoothL1Loss
    loss_cls:
      alpha: 0.25
      gamma: 2.0
      loss_weight: 1.0
      type: FocalLoss
      use_sigmoid: true
    loss_loc:
      alpha: 0.25
      gamma: 2.0
      loss_weight: 1.0
      type: FocalLoss
      use_sigmoid: true
    loss_shape:
      beta: 0.2
      loss_weight: 1.0
      type: BoundedIoULoss
    num_classes: 80
    square_anchor_generator:
      ratios:
      - 1.0
      scales:
      - 4
      strides:
      - 8
      - 16
      - 32
      - 64
      - 128
      type: AnchorGenerator
    stacked_convs: 4
    type: GARetinaHead
  neck:
    add_extra_convs: on_input
    in_channels:
    - 256
    - 512
    - 1024
    - 2048
    num_outs: 5
    out_channels: 256
    start_level: 1
    type: FPN
  test_cfg:
    max_per_img: 100
    min_bbox_size: 0
    nms:
      iou_threshold: 0.5
      type: nms
    nms_pre: 1000
    score_thr: 0.05
  train_cfg:
    allowed_border: -1
    assigner:
      ignore_iof_thr: -1
      min_pos_iou: 0.0
      neg_iou_thr: 0.5
      pos_iou_thr: 0.5
      type: MaxIoUAssigner
    center_ratio: 0.2
    debug: false
    ga_assigner:
      ignore_iof_thr: -1
      min_pos_iou: 0.4
      neg_iou_thr: 0.4
      pos_iou_thr: 0.5
      type: ApproxMaxIoUAssigner
    ga_sampler:
      add_gt_as_proposals: false
      neg_pos_ub: -1
      num: 256
      pos_fraction: 0.5
      type: RandomSampler
    ignore_ratio: 0.5
    pos_weight: -1
  type: RetinaNet
```

## [Training](#contents)

To train the model, run `train.py`.

### [Training process](#contents)

Standalone training mode:

```bash
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH]
```

* `CONFIG_PATH`: path to config file.

Training result will be stored in the path passed in config option `train_outputs`

## [Evaluation](#contents)

### [Evaluation process](#contents)

#### [Evaluation on GPU](#contents)

```shell
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [CHECKPOINT_PATH]
```

* `CONFIG_PATH`: path to config file.
* `CHECKPOINT_PATH`: path to checkpoint.

### [Evaluation result](#contents)

Result for GPU:

```log
CHECKING MINDRECORD FILES DONE!
Start Eval!
loading annotations into memory...
Done (t=0.48s)
creating index...
index created!

========================================

total images num:  5000
Processing, please wait a moment.
100%|██████████| 5000/5000 [0:59:32<00:00,  1.29it/s]
Loading and preparing results...
Converting ndarray to lists...
(138134, 7)
0/138134
DONE (t=0.92s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=27.15s).
Accumulating evaluation results...
DONE (t=4.65s).
Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.361
Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=1000 ] = 0.559
Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=1000 ] = 0.385
Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=1000 ] = 0.206
Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=1000 ] = 0.394
Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=1000 ] = 0.483
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.558
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=300 ] = 0.558
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=1000 ] = 0.558
Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=1000 ] = 0.404
Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=1000 ] = 0.599
Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=1000 ] = 0.702

mAP: 0.361
```

## [Model Description](#contents)

### [Performance](#contents)

| Parameters          | GPU                                      |
| ------------------- |------------------------------------------|
| Model Version       | GA-RetinaNet ResNet50                    |
| Resource            | NVIDIA GeForce RTX 3090 (x4)             |
| Uploaded Date       | 10/18/2023 (month/day/year)              |
| MindSpore Version   | 2.1.0                                    |
| Dataset             | COCO2017                                 |
| Pretrained          | ???                                      |
| Training Parameters | epoch = 12,  batch_size = 4 (per device) |
| Optimizer           | SGD (momentum)                           |
| Loss Function       | SmoothL1Loss                             |
| Speed               | ???                                      |
| Total time          | ???                                      |
| outputs             | mAP                                      |
| mAP                 | ???                                      |
| Model for inference | ???                                      |
| configuration       | ga_retinanet_r101_caffe_fpn_1x_coco.yaml |
| Scripts             |                                          |

| Parameters          | GPU                                      |
| ------------------- |------------------------------------------|
| Model Version       | GA-Faster RCNN ResNet101                 |
| Resource            | NVIDIA GeForce RTX 3090 (x4)             |
| Uploaded Date       | 10/18/2023 (month/day/year)              |
| MindSpore Version   | 2.1.0                                    |
| Dataset             | COCO2017                                 |
| Pretrained          | ???                                      |
| Training Parameters | epoch = 12,  batch_size = 4 (per device) |
| Optimizer           | SGD (momentum)                           |
| Loss Function       | SmoothL1Loss                             |
| Speed               | ???                                      |
| Total time          | ???                                      |
| outputs             | mAP                                      |
| mAP                 | ???                                      |
| Model for inference | ???                                      |
| configuration       | ga_faster_r101_caffe_fpn_1x_coco.yaml    |
| Scripts             |                                          |

## [Description of Random Situation](#contents)

We use random seed in train.py.

## [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models).
