# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import numpy as np


def select_single_mlvl(mlvl_tensors, batch_id):
    """Extract a multi-scale single image tensor from a multi-scale batch
    tensor based on batch index.

    Note: The default value of detach is True, because the proposal gradient
    needs to be detached during the training of the two-stage model. E.g
    Cascade Mask R-CNN.

    Args:
        mlvl_tensors (list[np.array]): Batch tensor for all scale levels,
           each is a 4D-tensor.
        batch_id (int): Batch index.
        detach (bool): Whether detach gradient. Default True.

    Returns:
        list[np.array]: Multi-scale single image tensor.
    """
    assert isinstance(mlvl_tensors, (list, tuple))
    num_levels = len(mlvl_tensors)

    mlvl_tensor_list = [
        mlvl_tensors[i][batch_id] for i in range(num_levels)
    ]
    return mlvl_tensor_list


def filter_scores_and_topk(scores, score_thr, topk):
    """Filter results using score threshold and topk candidates.

    Args:
        scores (np.array): The scores, shape (num_bboxes, K).
        score_thr (float): The score filter threshold.
        topk (int): The number of topk candidates.
        results (dict or list or np.array, Optional): The results to
           which the filtering rule is to be applied. The shape
           of each item is (num_bboxes, N).

    Returns:
        tuple: Filtered results

            - scores (np.array): The scores after being filtered, \
                shape (num_bboxes_filtered, ).
            - labels (np.array): The class labels, shape \
                (num_bboxes_filtered, ).
            - anchor_idxs (np.array): The anchor indexes, shape \
                (num_bboxes_filtered, ).
            - filtered_results (dict or list or np.array, Optional): \
                The filtered results. The shape of each item is \
                (num_bboxes_filtered, N).
    """
    valid_mask = scores > score_thr
    scores = scores[valid_mask]
    valid_idxs = np.nonzero(valid_mask)

    num_topk = min(topk, len(valid_idxs[0]))
    idxs = np.argsort(scores)[::-1][:num_topk]

    keep_idxs = valid_idxs[0][idxs]
    labals = valid_idxs[1][idxs]

    return keep_idxs, scores[idxs], labals
