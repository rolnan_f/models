# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Volume rendering pipeline."""

import mindspore as ms
from mindspore import nn
from mindspore import ops

from ..volume_rendering.scene_representation import NeRF
from .coordinates_samplers import HierarchicalCoordsBuilder


class Inference(nn.Cell):
    def __init__(self,
                 embedding_xyz,
                 embedding_dir,
                 typ,
                 nerf_config,
                 train_config,
                 chunk,
                 noise_std):
        super().__init__()
        self.embedding_xyz = embedding_xyz
        self.embedding_dir = embedding_dir
        self.model = NeRF(typ=typ,
                          depth=nerf_config["fine_net_depth"],
                          width=nerf_config["fine_net_width"],
                          in_channels_xyz=6 * nerf_config["n_emb_xyz"] + 3,
                          in_channels_dir=6 * nerf_config["n_emb_dir"] + 3,
                          encode_appearance=train_config["encode_a"],
                          in_channels_a=train_config["n_a"],
                          encode_random=train_config["encode_random"])
        self.chunk = chunk
        self.noise_std = noise_std
        self.cumprod = ops.CumProd()
        self.reduce_sum = ops.ReduceSum()
        self.concat = ops.Concat(axis=-1)
        self.ones = ops.Ones()

    def construct(self,
                  results,
                  xyz,
                  z_vals,
                  n_rays,
                  dir_embedded,
                  a_embedded=None,
                  a_embedded_random=None,
                  test_time=False):
        static_rgbs = None
        static_rgbs_random = None
        typ = self.model.typ
        n_samples_ = xyz.shape[1]
        xyz_ = ops.reshape(xyz, (-1, 3))

        # Perform model inference to get rgb and raw sigma
        output_random = a_embedded_random is not None
        out_chunks = []
        if typ == 'coarse' and test_time:
            for i in range(0, xyz_.shape[0], self.chunk):
                xyz_embedded = \
                    self.embedding_xyz.embed(xyz_[i:i + self.chunk])
                out_chunks += [self.model(xyz_embedded, sigma_only=True)]
            out = ops.cat(out_chunks, 0)
            static_sigmas = ops.reshape(out, (n_rays, n_samples_))
        else:  # infer rgb and sigma and others
            dir_embedded_ = dir_embedded.repeat_interleave(n_samples_, 0) \
                .reshape(-1, dir_embedded.shape[1])
            # create other necessary inputs
            a_embedded_ = None
            a_embedded_random_ = None
            if self.model.encode_appearance and a_embedded is not None:
                a_embedded_ = a_embedded.repeat_interleave(n_samples_, 0) \
                    .reshape(-1, a_embedded.shape[1])
            if output_random:
                a_embedded_random_ = \
                    a_embedded_random.repeat_interleave(n_samples_, 0) \
                    .reshape(-1, a_embedded_random.shape[1])

            for i in range(0, xyz_.shape[0], self.chunk):
                # inputs for original NeRF
                inputs = ops.concat(
                    (self.embedding_xyz.embed(xyz_[i:i + self.chunk]),
                     dir_embedded_[i:i + self.chunk]), axis=-1
                )
                # additional inputs for NeRF-W
                if self.model.encode_appearance and a_embedded_ is not None:
                    inputs = ops.concat(
                        (inputs, a_embedded_[i:i + self.chunk]),
                        axis=-1
                    )
                if output_random:
                    inputs = ops.concat(
                        (inputs, a_embedded_random_[i:i + self.chunk]),
                        axis=-1
                    )

                out_chunks += [self.model(inputs,
                                          output_random=output_random)]

            out = ops.cat(out_chunks, 0)
            out = ops.reshape(out, (n_rays, n_samples_, out.shape[-1]))
            static_rgbs = out[..., :3]  # (N_rays, N_samples_, 3)
            static_sigmas = out[..., 3]  # (N_rays, N_samples_)
            if output_random:
                static_rgbs_random = out[..., 4:]

        deltas = z_vals[..., 1:] - z_vals[..., :-1]
        delta_inf = 1e2 * self.ones((deltas.shape[0], 1), ms.float32)
        deltas = self.concat((deltas, delta_inf))  # [N_rays, N_samples]

        noise = \
            ops.randn_like(static_sigmas, dtype=ms.float32) * self.noise_std
        alphas = 1.0 - ops.exp((-ops.relu(static_sigmas + noise) * deltas))

        p_ones = self.ones((alphas.shape[0], 1), ms.float32)
        p_not_alpha = 1.0 - alphas + 1e-10
        concatenated_p = self.concat((p_ones, p_not_alpha))
        alpha_ray_sampling = self.cumprod(concatenated_p, -1)[:, :-1]
        weights = alphas * alpha_ray_sampling  # [N_rays, N_samples]
        weights_sum = self.reduce_sum(weights, -1)

        results[f'weights_{typ}'] = weights
        results[f'opacity_{typ}'] = weights_sum

        if test_time and typ == 'coarse':
            return results

        rgb_map = self.reduce_sum(
            weights[..., None] * static_rgbs, -2
        )  # [N_rays, 3]
        results[f'rgb_{typ}'] = rgb_map

        if output_random:
            rgb_map_random = ops.unsqueeze(weights, -1) * static_rgbs_random
            rgb_map_random = self.reduce_sum(rgb_map_random, 1)
            results[f'rgb_{typ}_random'] = rgb_map_random

        results[f'depth_{typ}'] = self.reduce_sum(weights * z_vals, -1)
        return results


class VolumeRendering(nn.Cell):
    def __init__(self,
                 embedding_xyz,
                 embedding_dir,
                 nerf_config,
                 train_config,
                 hierarchical_samples: int,
                 dtype: ms.Type = ms.float32,
                 chunk: int = 1024 * 32,
                 perturbation: float = 0,
                 n_samples: int = 64,
                 use_disp: bool = False,
                 noise_std: float = 1):
        super().__init__()
        self.hierarchical_samples = hierarchical_samples
        self.hierarchical_builder = HierarchicalCoordsBuilder(
            hierarchical_samples, bool(perturbation), dtype)
        self.encode_appearance = train_config["encode_a"]
        self.encode_random = train_config["encode_random"]
        self.inference_coarse = Inference(embedding_xyz=embedding_xyz,
                                          embedding_dir=embedding_dir,
                                          typ="coarse",
                                          nerf_config=nerf_config,
                                          train_config=train_config,
                                          chunk=chunk,
                                          noise_std=noise_std)
        self.inference_fine = Inference(embedding_xyz=embedding_xyz,
                                        embedding_dir=embedding_dir,
                                        typ="fine",
                                        nerf_config=nerf_config,
                                        train_config=train_config,
                                        chunk=chunk,
                                        noise_std=noise_std)
        self.embedding_dir = embedding_dir
        self.n_samples = n_samples
        self.perturbation = perturbation
        self.use_disp = use_disp

        self.ones_like = ops.OnesLike()
        self.sort = ops.Sort(-1)
        self.concat = ops.Concat(-1)

    def construct(self,
                  rays,
                  a_embedded_from_img=None,
                  a_embedded_random=None,
                  test_time=False):

        n_rays = rays.shape[0]
        rays_o, rays_d = rays[:, 0:3], rays[:, 3:6]  # both (N_rays, 3)
        near, far = rays[:, 6:7], rays[:, 7:8]  # both (N_rays, 1)
        # Embed direction
        dir_embedded = self.embedding_dir.embed(rays_d)

        rays_o = ops.reshape(rays_o, (rays_o.shape[0], 1, rays_o.shape[1]))
        rays_d = ops.reshape(rays_d, (rays_o.shape[0], 1, rays_d.shape[1]))

        # Sample depth points
        z_steps = ops.linspace(0, 1, self.n_samples)
        if not self.use_disp:  # use linear sampling in depth space
            z_vals = near * (1.0 - z_steps) + far * z_steps
        else:  # use linear sampling in disparity space
            z_vals = 1.0 / (1.0 / near * (1.0 - z_steps) +
                            1.0 / far * z_steps)

        z_vals = z_vals.expand_as(ops.ones((n_rays, self.n_samples)))

        if self.perturbation > 0:  # perturb sampling depths (z_vals)
            # (N_rays, N_samples-1) interval mid points
            z_vals_mid = 0.5 * (z_vals[..., :-1] + z_vals[..., 1:])
            # get intervals between samples
            upper = ops.cat([z_vals_mid, z_vals[:, -1:]], -1)
            lower = ops.cat([z_vals[:, :1], z_vals_mid], -1)

            perturb_rand = ms.numpy.rand(z_vals.shape, dtype=ms.float32)
            z_vals = lower + (upper - lower) * perturb_rand
        xyz_coarse = rays_o + rays_d * ops.unsqueeze(z_vals, -1)

        results = {}
        a_embedded = None
        results = self.inference_coarse(
            results=results,
            xyz=xyz_coarse,
            z_vals=z_vals,
            n_rays=n_rays,
            dir_embedded=dir_embedded,
            a_embedded=a_embedded,
            a_embedded_random=None,
            test_time=test_time)

        if self.hierarchical_samples > 0:  # sample points for fine model
            # (N_rays, N_samples-1) interval mid points
            z_vals_mid = 0.5 * (z_vals[:, :-1] + z_vals[:, 1:])
            z_vals_ = \
                self.hierarchical_builder.build_coords(
                    z_vals_mid,
                    results['weights_coarse'][:, 1:-1]
                )
            z_vals_ = ops.stop_gradient(z_vals_)
            z_vals = ops.sort(ops.cat([z_vals, z_vals_], -1), -1)[0]
            xyz_fine = rays_o + rays_d * ops.unsqueeze(z_vals, -1)

            a_embedded_random_ = None
            if self.encode_appearance and a_embedded_from_img is not None:
                a_embedded = \
                    a_embedded_from_img.tile((xyz_fine.shape[0], 1))

            if self.encode_random and a_embedded_random is not None:
                a_embedded_random_ = \
                    a_embedded_random.tile((xyz_fine.shape[0], 1))

            results = self.inference_fine(
                results=results,
                xyz=xyz_fine,
                z_vals=z_vals,
                n_rays=n_rays,
                dir_embedded=dir_embedded,
                a_embedded=a_embedded,
                a_embedded_random=a_embedded_random_,
                test_time=test_time)

        return results
