# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Eval wrapper."""

from mindspore import nn


class WithEvalCell(nn.Cell):

    def __init__(self,
                 train_config,
                 loss,
                 network,
                 optimizer):
        super(WithEvalCell, self).__init__()
        self.loss = loss
        self.train_config = train_config
        self.network = network
        self.optimizer = optimizer

    def construct(self, *inputs):
        rays, _, rgbs, whole_img, _, _ = inputs
        a_embedded_from_img = self.network.enc_a(whole_img)
        results = self.network.network(rays[:self.train_config["chunk"]],
                                       a_embedded_from_img)
        results["a_embedded"] = a_embedded_from_img.copy()
        rgbs = rgbs[:self.train_config["chunk"]]
        loss = self.loss(results, rgbs,
                         self.train_config, self.optimizer.global_step)
        pred = results["rgb_fine"]

        return loss, pred, rgbs
