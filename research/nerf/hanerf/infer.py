# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Inference based on the scene config, poses and trained model."""

import argparse
import json
from pathlib import Path
import imageio

import numpy as np
from PIL import Image
from tqdm import tqdm

import mindspore as ms
from mindspore import load_checkpoint, load_param_into_net, ops
from mindspore.dataset import vision

from src.tools.rays import get_rays, recalculate_rays_to_ndc
from src.models.nerf_system import NeRFSystem

DEFAULT_DS_CONFIG = Path('src') / 'configs' / 'phototourism_ds_config.json'
DEFAULT_MODEL_CONFIG = Path('src') / 'configs' / 'nerf_config.json'


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--scene-name', type=str, default="brandenburg_gate",
                        choices=["brandenburg_gate", "trevi_fountain"],
                        help='Scene name.')
    parser.add_argument('--pose', type=Path, required=True,
                        help='Init pose as camera to world matrix.')
    parser.add_argument('--scene-config', type=Path, required=True,
                        help='Scene config. '
                             'Contains at least: '
                             '* image size - width and heights;'
                             '* frame intrinsic matrix;'
                             '* near and far planes;'
                             '* white background or not;'
                             'Config can be obtained as the result of NeRF '
                             'training.')
    parser.add_argument('--data-config', type=str,
                        default=DEFAULT_DS_CONFIG,
                        help='Path to the dataset config.')
    parser.add_argument('--train-config', type=str,
                        required=True,
                        help='Path to the train config.')
    parser.add_argument('--model-config', type=str,
                        default=DEFAULT_MODEL_CONFIG,
                        help='Volume rendering model config.')
    parser.add_argument('--model-ckpt', type=str, required=True,
                        help='Model checkpoints.')
    parser.add_argument('--hallucinate-image-path', type=str, required=True,
                        help='Image path to apply hallucinate scene.')
    parser.add_argument('--out-path', type=Path, required=True,
                        help='Path for output data saving:'
                             'predicted image, configs, optionally video etc.')
    parser.add_argument('--export-video', action='store_true', default=False,
                        help='Build final video.')
    parser.add_argument('--mode', choices=['graph', 'pynative'],
                        default='graph',
                        help='Model representation mode. '
                             'Pynative for debugging.')
    return parser.parse_args()


def euler_angles_to_rotation_matrix(theta):
    r_x = np.array([[1, 0, 0],
                    [0, np.cos(theta[0]), -np.sin(theta[0])],
                    [0, np.sin(theta[0]), np.cos(theta[0])]
                    ])
    r_y = np.array([[np.cos(theta[1]), 0, np.sin(theta[1])],
                    [0, 1, 0],
                    [-np.sin(theta[1]), 0, np.cos(theta[1])]
                    ])

    r_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
                    [np.sin(theta[2]), np.cos(theta[2]), 0],
                    [0, 0, 1]
                    ])
    r = np.dot(r_z, np.dot(r_y, r_x))
    return r


def define_poses_brandenburg_gate(pose_init):
    n_frames = 30 * 8

    dx1 = np.linspace(-0.25, 0.25, n_frames)
    dx2 = np.linspace(0.25, 0.38, n_frames - n_frames // 2)
    dx = np.concatenate((dx1, dx2))
    dy1 = np.linspace(0.05, -0.1, n_frames // 2)
    dy2 = np.linspace(-0.1, 0.05, n_frames - n_frames // 2)
    dy = np.concatenate((dy1, dy2))
    dz1 = np.linspace(0.1, 0.3, n_frames // 2)
    dz2 = np.linspace(0.3, 0.1, n_frames - n_frames // 2)
    dz = np.concatenate((dz1, dz2))
    theta_x1 = np.linspace(np.pi / 30, 0, n_frames // 2)
    theta_x2 = np.linspace(0, np.pi / 30, n_frames - n_frames // 2)
    theta_x = np.concatenate((theta_x1, theta_x2))
    theta_y = np.linspace(np.pi / 10, -np.pi / 10, n_frames)
    theta_z = np.linspace(0, 0, n_frames)
    poses_test = np.tile(pose_init, (n_frames, 1, 1))
    for i in range(n_frames):
        poses_test[i, 0, 3] += dx[i]
        poses_test[i, 1, 3] += dy[i]
        poses_test[i, 2, 3] += dz[i]
        poses_test[i, :, :3] = \
            np.dot(euler_angles_to_rotation_matrix([theta_x[i],
                                                    theta_y[i],
                                                    theta_z[i]]),
                   poses_test[i, :, :3])
    return poses_test


def define_poses_trevi_fountain(pose_init):
    n_frames = 30 * 8

    dx = np.linspace(-0.8, 0.7, n_frames)  # + right

    dy1 = np.linspace(-0., 0.05, n_frames // 2)  # + down
    dy2 = np.linspace(0.05, -0., n_frames - n_frames // 2)
    dy = np.concatenate((dy1, dy2))

    dz1 = np.linspace(0.4, 0.1, n_frames // 4)
    dz2 = np.linspace(0.1, 0.5, n_frames // 4)
    dz3 = np.linspace(0.5, 0.1, n_frames // 4)
    dz4 = np.linspace(0.1, 0.4, n_frames - 3 * (n_frames // 4))
    dz = np.concatenate((dz1, dz2, dz3, dz4))

    theta_x1 = np.linspace(-0, 0, n_frames // 2)
    theta_x2 = np.linspace(0, -0, n_frames - n_frames // 2)
    theta_x = np.concatenate((theta_x1, theta_x2))

    theta_y = np.linspace(np.pi / 6, -np.pi / 6, n_frames)

    theta_z = np.linspace(0, 0, n_frames)

    poses_test = np.tile(pose_init, (n_frames, 1, 1))
    for i in range(n_frames):
        poses_test[i, 0, 3] += dx[i]
        poses_test[i, 1, 3] += dy[i]
        poses_test[i, 2, 3] += dz[i]
        poses_test[i, :, :3] = \
            np.dot(euler_angles_to_rotation_matrix([theta_x[i],
                                                    theta_y[i],
                                                    theta_z[i]]),
                   poses_test[i, :, :3])
    return poses_test


def predict_cam2world_image(volume_renderer,
                            cam2world,
                            scene_config,
                            rays_batch,
                            a_embedded_from_img):
    intrinsic = np.reshape(np.array(scene_config['intrinsic']), (3, 3))
    w, h = scene_config['width'], scene_config['height']

    # Build rays.
    rays_o, rays_d, rays_raw_d = get_rays(width=w,
                                          height=h,
                                          intrinsic=intrinsic,
                                          c2w=cam2world)
    if scene_config['is_ndc']:
        rays_o, rays_d = recalculate_rays_to_ndc(height=h,
                                                 width=w,
                                                 focal=scene_config['focal'],
                                                 near=1.0,
                                                 rays_o=rays_o,
                                                 rays_d=rays_d)
    rgb_coarse = []
    rgb_fine = []
    depth_fine = []
    weight_fine = []

    rays_o = ms.Tensor(rays_o, dtype=ms.float32)
    rays_raw_d = ms.Tensor(rays_raw_d, dtype=ms.float32)
    for i in range(0, len(rays_o), rays_batch):
        end = i + rays_batch
        r_o = rays_o[i: end]
        raw_r_d = rays_raw_d[i: end]
        nears = scene_config["near"] * ops.ones_like(r_o[:, :1])
        fars = scene_config["far"] * ops.ones_like(r_o[:, :1])
        rays = ms.ops.Concat(axis=-1)((r_o, raw_r_d, nears, fars))
        res = volume_renderer(rays, a_embedded_from_img)
        rgb_coarse.append(res["rgb_coarse"].asnumpy())
        rgb_fine.append(res["rgb_fine"].asnumpy())
        depth_fine.append(res["depth_fine"].asnumpy())
        weight_fine.append(res["weights_fine"].asnumpy())
    rgb_coarse = np.reshape(np.concatenate(rgb_coarse, axis=0), (h, w, 3))
    rgb_fine = np.reshape(np.concatenate(rgb_fine, axis=0), (h, w, 3))
    depth_fine = np.reshape(np.concatenate(depth_fine, axis=0), (h, w, -1))
    weights = np.reshape(np.concatenate(weight_fine, axis=0), (h, w, -1))
    return rgb_coarse, rgb_fine, depth_fine, weights


def main():
    args = parse_args()

    mode = ms.GRAPH_MODE if args.mode == 'graph' else ms.PYNATIVE_MODE
    ms.set_context(mode=mode,
                   device_target='GPU')

    # Scene config.
    with open(args.scene_config, 'r') as f:
        scene_config = json.load(f)
    # Poses config.
    with open(args.pose, 'r') as f:
        cam2world = json.load(f)
        pose_init = np.reshape(np.array(cam2world), (-1, 3, 4))
    # Model config.
    with open(args.model_config, 'r') as f:
        model_config = json.load(f)

    # Output directory.
    args.out_path.mkdir(parents=True, exist_ok=True)

    scene_config_out = args.out_path / 'scene_config.json'
    with open(scene_config_out, 'w') as f:
        json.dump(scene_config, f, indent=4)

    model_config_out = args.out_path / 'model_config.json'
    with open(model_config_out, 'w') as f:
        json.dump(model_config, f, indent=4)

    # Build model and load weights.
    with open(args.train_config, 'r') as f:
        train_cfg = json.load(f)
    with open(args.model_config, 'r') as f:
        model_cfg = json.load(f)

    model = NeRFSystem(train_config=train_cfg,
                       nerf_config=model_cfg)
    param_dict = load_checkpoint(args.model_ckpt)
    load_param_into_net(model, param_dict)

    org_img = Image.open(args.hallucinate_image_path).convert('RGB')
    img_downscale = 8
    img_w, img_h = org_img.size
    img_w = img_w // img_downscale
    img_h = img_h // img_downscale
    img = org_img.resize((img_w, img_h), Image.LANCZOS)
    transform = vision.ToTensor()
    normalize = vision.Normalize(mean=[0.5, 0.5, 0.5],
                                 std=[0.5, 0.5, 0.5],
                                 is_hwc=False)
    img = normalize(transform(img))  # (3, h, w)
    whole_img = ms.Tensor(img, dtype=ms.float32)
    whole_img = whole_img.unsqueeze(0)
    a_embedded_from_img = model.enc_a(whole_img)

    if args.scene_name == "brandenburg_gate":
        poses = define_poses_brandenburg_gate(pose_init)
    elif args.scene_name == "trevi_fountain":
        poses = define_poses_trevi_fountain(pose_init)

    fine_images = []
    disp_maps = []

    for pose_idx in tqdm(range(poses.shape[0])):
        pose = poses[pose_idx]
        pose = np.concatenate((pose, np.array([[0.0, 0.0, 0.0, 1.0]])), axis=0)
        rgb_coarse, rgb_fine, depth_fine, weights = predict_cam2world_image(
            model.network,
            pose,
            scene_config,
            train_cfg["batch_size"],
            a_embedded_from_img
        )
        # Save images and arrays.
        np.save(args.out_path / f'{pose_idx}_rgb_fine', rgb_fine)
        np.save(args.out_path / f'{pose_idx}_depth_fine', depth_fine)
        np.save(args.out_path / f'{pose_idx}_weight_fine', weights)

        img_coarse = np.uint8(np.clip(rgb_coarse, 0, 1) * 255)
        imageio.imwrite(args.out_path / f'{pose_idx}_image_coarse.png',
                        img_coarse)
        img_fine = np.uint8(np.clip(rgb_fine, 0, 1) * 255)
        imageio.imwrite(args.out_path / f'{pose_idx}_image_fine.png', img_fine)
        if args.export_video:
            fine_images.append(img_fine)
            disp_map = 1.0 / np.maximum(
                1e-10 * np.ones_like(depth_fine),
                depth_fine / (np.sum(weights, -1, keepdims=True) + 1e-6)
            )
            disp_map = np.uint8(
                np.clip(disp_map / np.max(disp_map), 0, 1) * 255
            )
            disp_maps.append(disp_map)

    if args.export_video:
        rgb_video = args.out_path / 'rgb_video.mp4'
        imageio.mimwrite(rgb_video, fine_images, fps=30, quality=8)

        disp_video = args.out_path / 'disp_video.mp4'
        imageio.mimwrite(disp_video, disp_maps, fps=30, quality=8)


if __name__ == '__main__':
    main()
