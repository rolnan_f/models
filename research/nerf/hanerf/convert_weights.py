# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Convert weights from PyTorch Ha-NeRF repo to MindSpore."""

import argparse
import json
from pathlib import Path

import mindspore as ms
from mindspore import save_checkpoint
from mindspore.train.serialization import load_param_into_net

import torch

from src.models.nerf_system import NeRFSystem

DEFAULT_MODEL_CONFIG = Path('src') / 'configs' / 'nerf_config.json'


def replace_underline_to_point(input_string):
    index = input_string.rfind("_")
    if index != -1:
        digit = input_string[index + 1]
        if digit.isdigit():
            replacement = f".{int(digit) - 1}"
            output_string = \
                input_string[:index] + replacement + input_string[index + 2:]
            return output_string
    return input_string


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--model-config', type=Path,
                        default=DEFAULT_MODEL_CONFIG,
                        help='Path to the model config.')
    parser.add_argument('--train-config', type=Path,
                        required=True,
                        help='Path to the train config.')
    parser.add_argument('--torch-weights', type=Path,
                        help='Torch checkpoint file.')
    parser.add_argument('--out-weights', type=Path,
                        help='Output mindspore checkpoint .ckpt file.')
    return parser.parse_args()


def load_torch_weights(model, torch_ckpt_path: Path, out_ckpt_path: Path):
    ckpt = torch.load(torch_ckpt_path)

    network_params = {}
    for name, w in ckpt.items():
        if "coarse" in name or "fine" in name:
            if "coarse" in name:
                layer_name = \
                    name.replace('nerf_coarse',
                                 'network.inference_coarse.model')
            elif "fine" in name:
                layer_name = \
                    name.replace('nerf_fine',
                                 'network.inference_fine.model')
            layer_name = layer_name.replace(".0", "")
            if "xyz_encoding" in layer_name and "final" not in layer_name:
                layer_name = replace_underline_to_point(layer_name)
        else:
            layer_name = name
            if "embedding_view" in name:
                layer_name = 'embedding_view.embedding_table'
            network_params[layer_name] = \
                ms.Parameter(w.cpu().numpy(), name=name)
        network_params[layer_name] = ms.Parameter(w.cpu().numpy(), name=name)

    load_param_into_net(model, network_params)
    save_checkpoint(model, str(out_ckpt_path))


def main():
    args = parse_args()
    with open(args.train_config, 'r') as f:
        train_cfg = json.load(f)
    with open(args.model_config, 'r') as f:
        model_cfg = json.load(f)
    nerf = NeRFSystem(train_config=train_cfg,
                      nerf_config=model_cfg)
    load_torch_weights(model=nerf,
                       torch_ckpt_path=args.torch_weights,
                       out_ckpt_path=args.out_weights)


if __name__ == '__main__':
    main()
