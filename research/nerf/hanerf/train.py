# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""HA-NeRF train script."""

import argparse
import json
import os
from pathlib import Path
from typing import List

import numpy as np
import mindspore as ms
import mindspore.nn as nn
from mindspore import context
from mindspore.communication.management import init, get_rank
from mindspore.context import ParallelMode
from mindspore.train.serialization import (
    load_checkpoint, load_param_into_net
)

from src.data.phototourism_mask_grid_sample import create_datasets
from src.tools.common import get_callbacks
from src.models import NeRFSystem
from src.trainer import TrainOneStepCell, WithEvalCell
from src.tools.metrics import PSNR
from src.tools.criterion import HaNeRFLoss
from src.tools.mlflow_funcs import mlflow_log_state, mlflow_log_args

DEFAULT_DS_CONFIG = Path('src') / 'configs' / 'phototourism_ds_config.json'
DEFAULT_MODEL_CONFIG = Path('src') / 'configs' / 'nerf_config.json'


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    dataset = parser.add_argument_group('Dataset arguments.')
    dataset.add_argument('--data-path', type=str, required=True,
                         help='Dataset directory.')
    dataset.add_argument('--data-config', type=Path,
                         default=DEFAULT_DS_CONFIG,
                         help='Path to the dataset config.')

    train = parser.add_argument_group('Training arguments.')
    train.add_argument('--train-config', type=Path,
                       required=True,
                       help='Path to the train config.')
    train.add_argument('--model-config', type=Path,
                       default=DEFAULT_MODEL_CONFIG,
                       help='Path to the model config.')
    train.add_argument('--init-ckpt', type=Path, default=None,
                       help='Path with pretrained model ckpt.')
    train.add_argument('--out-dir', type=Path, required=True,
                       help='Directory to save training model meta info.')
    train.add_argument('--mode', choices=['graph', 'pynative'],
                       default='graph',
                       help='Model representation mode. '
                            'Pynative for debugging.')

    runner = parser.add_argument_group('Runner context.')
    runner.add_argument('--num-workers', default=1, type=int,
                        help='Dataset number of parallel workers.')
    runner.add_argument('--device-type', default='GPU', type=str,
                        choices=['GPU', 'CPU'],
                        help='Training device.')
    runner.add_argument('--gpu-devices-num', type=int, default=1,
                        help='GPU devices number.')
    runner.add_argument('--gpu-devices-id', type=int, nargs='*',
                        default=[0],
                        help='GPU devices id.')

    args = parser.parse_args()
    if len(args.gpu_devices_id) != args.gpu_devices_num:
        args.gpu_devices_id = list(range(args.gpu_devices_num))
    return args


def load_ckpt(model, pretrained_weights, exclude_epoch_state=True):
    if os.path.isfile(pretrained_weights):
        print(f'=> loading pretrained weights from {pretrained_weights}')
        param_dict = load_checkpoint(pretrained_weights)
        last_epoch = 0
        if exclude_epoch_state:
            if 'epoch_num' in param_dict:
                last_epoch = int(param_dict['epoch_num'].asnumpy())
                param_dict.pop('epoch_num')
            if 'step_num' in param_dict:
                param_dict.pop('step_num')
            if 'learning_rate' in param_dict:
                param_dict.pop('learning_rate')
        load_param_into_net(model, param_dict)
    else:
        raise IOError(f'=> no pretrained weights found at '
                      f'{pretrained_weights}.')
    return last_epoch


def set_device(device_target: str,
               devices_num: int,
               devices_id: List[int]):
    """Set device and turn on distributed mode (if device_num > 1)"""
    if 'DEVICE_NUM' not in os.environ.keys():
        os.environ['DEVICE_NUM'] = str(devices_num)
        os.environ['RANK_SIZE'] = str(devices_num)

    env_device_num = int(os.environ.get('DEVICE_NUM', 1))
    rank = 0
    if device_target == 'GPU':
        if env_device_num > 1:
            init(backend_name='nccl')
            context.reset_auto_parallel_context()
            context.set_auto_parallel_context(
                device_num=env_device_num,
                parallel_mode=ParallelMode.DATA_PARALLEL,
                gradients_mean=True)
            rank = get_rank()
        else:
            context.set_context(device_target=device_target,
                                device_id=devices_id[rank])
    elif device_target == 'CPU':
        context.set_context(device_target=device_target)
    else:
        raise ValueError('Unsupported platform.')
    return rank


def set_context(mode: bool = False,
                device_target: str = 'GPU',
                devices_num: int = 1,
                devices_id=(0,)):
    ms_mode = ms.GRAPH_MODE if mode == 'graph' else ms.PYNATIVE_MODE
    context.set_context(
        mode=ms_mode,
        device_target=device_target,
        enable_graph_kernel=False
    )
    rank = set_device(device_target,
                      devices_num,
                      devices_id)
    return rank


def gen_lr(lr, train_epochs, ds_size, min_lr=1e-7):
    steps = train_epochs * ds_size
    # CosineAnnealingLR
    learning_rate = [
        min_lr + 0.5 * (lr - min_lr) *
        (1 + np.cos((np.pi * (step / ds_size))))
        for step in range(steps)
    ]
    return learning_rate


def copy_configs_out(out_dir, model_cfg, train_cfg, data_cfg):
    # Copy configs to the output directory.
    out_dir.mkdir(parents=True, exist_ok=True)
    model_config_out = out_dir / 'model_config.json'
    with open(model_config_out, 'w') as f:
        json.dump(model_cfg, f, indent=4)
    train_config_out = out_dir / 'train_config.json'
    with open(train_config_out, 'w') as f:
        json.dump(train_cfg, f, indent=4)
    data_config_out = out_dir / 'data_config.json'
    with open(data_config_out, 'w') as f:
        json.dump(data_cfg, f, indent=4)


def main():
    args = parse_args()
    with open(args.train_config, 'r') as f:
        train_cfg = json.load(f)
    with open(args.data_config, 'r') as f:
        data_cfg = json.load(f)
    with open(args.model_config, 'r') as f:
        model_cfg = json.load(f)
    # Set context.
    rank = set_context(
        args.mode, args.device_type,
        args.gpu_devices_num, args.gpu_devices_id)

    mlflow_log_state()
    mlflow_log_args(args.__dict__)
    mlflow_log_args(train_cfg)
    mlflow_log_args(data_cfg)
    mlflow_log_args(model_cfg)

    # Dataset.
    train_ds, val_ds = create_datasets(ds_path=args.data_path,
                                       img_downscale=data_cfg["img_downscale"],
                                       use_cache=data_cfg["use_cache"],
                                       batch_size=train_cfg["batch_size"],
                                       min_scale=train_cfg["min_scale"],
                                       num_parallel_workers=args.num_workers,
                                       num_shards=args.gpu_devices_num,
                                       shard_id=rank
                                       )
    copy_configs_out(
        args.out_dir, model_cfg, train_cfg, data_cfg)

    print('Start the model initialization.')

    # Create model and load checkpoints.
    network = NeRFSystem(train_config=train_cfg,
                         nerf_config=model_cfg)

    if args.init_ckpt is not None:
        ckpt_last_epoch = load_ckpt(network, str(args.init_ckpt))
    else:
        ckpt_last_epoch = 0

    lr = gen_lr(train_cfg['lr'], train_cfg['epochs'],
                train_ds.get_dataset_size(), train_cfg["min_lr"])
    opt = nn.Adam(
        params=network.trainable_params(), learning_rate=ms.Tensor(lr))

    loss = HaNeRFLoss(train_cfg, coef=1)
    net_with_loss = TrainOneStepCell(train_cfg,
                                     loss,
                                     network,
                                     opt)
    eval_network = WithEvalCell(train_cfg, loss, network, opt)
    model = ms.Model(
        net_with_loss, optimizer=opt, metrics={'PSNR': PSNR()},
        eval_network=eval_network, eval_indexes=[0, 1, 2])
    # Callbacks.
    cb = get_callbacks(
        'nerf', rank, train_ds.get_dataset_size(),
        val_ds.get_dataset_size(), args.out_dir, log_path=args.out_dir,
        ckpt_save_every_step=train_ds.get_dataset_size() // 2 - 1,
        ckpt_save_every_sec=0, ckpt_keep_num=train_cfg['epochs'] * 2,
        print_loss_every=100, collect_graph=True)
    # Train.
    epochs = train_cfg['epochs']
    if ckpt_last_epoch < epochs:
        print(f'Start training from {ckpt_last_epoch}.')
        model.fit(epochs, train_ds, callbacks=cb,
                  valid_dataset=val_ds, initial_epoch=ckpt_last_epoch)


if __name__ == '__main__':
    main()
