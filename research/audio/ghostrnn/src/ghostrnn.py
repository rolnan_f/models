# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===========================================================================
"""GhostRNN network."""
from __future__ import absolute_import

import math
import numpy as np

import mindspore
import mindspore.ops as P
from mindspore import nn
from mindspore.common.tensor import Tensor
from mindspore.common.parameter import Parameter
from mindspore.common.initializer import initializer, Uniform
from mindspore import context
from mindspore.nn.cell import Cell

context.set_context(mode=context.PYNATIVE_MODE)

class GhostRNNKWS(Cell):
    '''Build GhostRNN network for KWS'''
    def __init__(self, model_settings, model_size_info):
        super().__init__()
        # Batch Time_step Feature
        self.print = mindspore.ops.Print()  # For debug
        self.label_count = model_settings.model_setting_label_count
        self.input_frequency_size = model_settings.model_setting_dct_coefficient_count
        self.input_time_size = model_settings.model_setting_spectrogram_length
        self.hidden_size = model_size_info.hidden_size
        self.ghost_ratio = model_size_info.ghost_ratio

        # Ghost RNN
        self.ghost_rnn = GhostRNNCell(input_size=self.input_frequency_size,
                                      hidden_size=self.hidden_size, ghost_ratio=self.ghost_ratio, has_bias=True)

        # Linear
        self.linear = nn.Dense(model_size_info.hidden_size, self.label_count)

    def construct(self, x):
        # Init hx for GhostRNN
        hx = mindspore.ops.zeros((x.shape[0], self.hidden_size), mindspore.float32)

        # Loop for the time step
        for i in range(self.input_time_size):
            hx = self.ghost_rnn(x[:, i], hx)

        # Classify at the last time step
        y = self.linear(hx)
        return y


class GhostRNNCell(Cell):
    r"""
    A GhostRNN cell.
    """
    def __init__(self, input_size: int, hidden_size: int, ghost_ratio: int, has_bias: bool):
        super().__init__()

        self.print = mindspore.ops.Print()  # For debug
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.ghost_ratio = ghost_ratio
        self.ghost_size = hidden_size - (hidden_size // ghost_ratio)
        self.has_bias = has_bias
        self.weight_gate = Parameter(
            Tensor(np.random.randn(2 * (hidden_size // ghost_ratio), input_size + hidden_size).astype(np.float32)))
        self.weight_candidate = Parameter(
            Tensor(np.random.randn((hidden_size // ghost_ratio), input_size + hidden_size).astype(np.float32)))
        self.weight_ghost = Parameter(Tensor(np.random.randn(self.ghost_size,
                                                             (hidden_size // ghost_ratio)).astype(np.float32)))
        if has_bias:
            self.bias_gate = Parameter(Tensor(np.random.randn(2 * (hidden_size // ghost_ratio)).astype(np.float32)))
            self.bias_candidate = Parameter(Tensor(np.random.randn((hidden_size // ghost_ratio)).astype(np.float32)))
            self.bias_ghost = Parameter(Tensor(np.random.randn(self.ghost_size).astype(np.float32)))
        else:
            self.bias_gate = None
            self.bias_candidate = None
            self.bias_ghost = None
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1 / math.sqrt(self.hidden_size)
        for weight in self.get_parameters():
            weight.set_data(initializer(Uniform(stdv), weight.shape))

    def construct(self, x, hx):
        return _ghostgru_cell(x, hx, self.weight_gate, self.weight_candidate, self.weight_ghost,
                              self.bias_gate, self.bias_candidate, self.bias_ghost)


def _ghostgru_cell(inputs, hidden, weight_gate, weight_candidate, weight_ghost, bias_gate, bias_candidate, bias_ghost):
    '''ghostgru cell function'''
    # Define concat op
    concat_op = mindspore.ops.Concat(1)

    # Calculate the gate
    if bias_gate is None:
        gate_inputs = P.MatMul(False, True)(concat_op([inputs, hidden]), weight_gate)
    else:
        gate_inputs = P.MatMul(False, True)(concat_op([inputs, hidden]), weight_gate) + bias_gate
    value = P.Sigmoid()(gate_inputs)
    r, u = P.Split(1, 2)(value)
    r_state = r * hidden[:, bias_ghost.shape[0]:]

    # Calculate the candidate
    if bias_candidate is None:
        candidate = P.MatMul(False, True)(concat_op([inputs, hidden[:, :bias_ghost.shape[0]], r_state]),
                                          weight_candidate)
    else:
        candidate = P.MatMul(False, True)(concat_op([inputs, hidden[:, :bias_ghost.shape[0]], r_state]),
                                          weight_candidate) + bias_candidate
    c = P.Tanh()(candidate)

    # Calculate the new state
    new_h = u * hidden[:, bias_ghost.shape[0]:] + (1 - u) * c

    # Calculate the ghost state
    ghost_state = P.MatMul(False, True)(new_h, weight_ghost) + bias_ghost
    ghost_state = P.Tanh()(ghost_state)

    # Cancat the ghost state and new state
    new_h_with_ghost = concat_op([ghost_state, new_h])

    return new_h_with_ghost
