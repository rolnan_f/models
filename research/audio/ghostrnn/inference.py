# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===========================================================================
"""GhostRNN eval example."""
import numpy as np
import mindspore
from mindspore import Tensor, Model
from mindspore.common import dtype as mstype

from src.ghostrnn import GhostRNNKWS
from src.config import config


def main():
    # Get data of feature mfcc
    audio_feature = np.load(config.data_path)
    audio_feature = audio_feature.reshape((-1, 49, 10))

    # Define net
    network = GhostRNNKWS(config, config.model_size_info)

    # Load Checkpoint
    checkpoint_file_path = config.checkpoint_url
    param_dict = mindspore.load_checkpoint(checkpoint_file_path, strict_load=True)
    mindspore.load_param_into_net(network, param_dict)

    # Evaluate
    model = Model(network)
    output = model.predict(Tensor(audio_feature, mstype.float32))
    output = output.asnumpy()
    top1_output = np.argmax(output, (-1))
    print(f"The prediction is {top1_output}")

if __name__ == "__main__":
    main()
